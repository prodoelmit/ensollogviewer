#include "batteriestablewindow.h"
#include <QHeaderView>
#include "enums.h"
#include <QMap>
#include <QList>
#include "lvapp.h"
#include <QMenuBar>
#include <QMessageBox>

#include "qdebug.h"
#include <QTime>



namespace logviewer {
BatteriesTableWindow::BatteriesTableWindow()
	: QMainWindow()
{


	initActions();
	initMenu();
    initLayout();
    createTable();
    fillTable();








}

void BatteriesTableWindow::initLayout()
{
	m_cWidget = new QWidget();
    m_layout = new QGridLayout();
	m_cWidget->setLayout(m_layout);
	setCentralWidget(m_cWidget);

	m_nilToolbar = new QToolBar();
	m_batteryToolbar = new QToolBar();
	m_batteryToolbar->addAction(m_showCurDataAction);
    m_batteryToolbar->addAction(m_showChartAction);
    m_batteryToolbar->addAction(m_showDischargeStatsAction);
    addToolBar(m_batteryToolbar);
	addToolBar(m_nilToolbar);
	m_nilToolbar->hide();
	m_batteryToolbar->show();




}

void BatteriesTableWindow::initMenu()
{

	auto b = menuBar();
    QMenu* dbMenu = new QMenu(tr("&База данных"));
	dbMenu->addAction(m_importCurDataAction);
	dbMenu->addAction(m_importLifeDataAction);
	dbMenu->addAction(m_dropMongoAction);


	b->addMenu(dbMenu);

}

void BatteriesTableWindow::initActions()
{

    m_showCurDataAction = new QAction("Все события");
	connect(m_showCurDataAction, &QAction::triggered,
			this, &BatteriesTableWindow::showCurData);
	m_showCurDataAction->setDisabled(true);

    m_importCurDataAction = new QAction("Импортировать файл CURRENT.LOG");
	connect(m_importCurDataAction, &QAction::triggered,
			app, &LVApp::importCurData);
    m_importLifeDataAction = new QAction("Импортировать файл LIFE.LOG");
	connect(m_importLifeDataAction, &QAction::triggered,
			app, &LVApp::importLifeData);

    m_dropMongoAction = new QAction("Очистить базу данных");
	connect(m_dropMongoAction, &QAction::triggered,
			[this](){
        int r = QMessageBox::question(this, "Внимание!", "Вы уверены, что хотите очистить таблицу? Это приведет к уничтожению logviewer.curData и logviewer.lifeData", QMessageBox::No, QMessageBox::Yes, QMessageBox::NoButton);
		if (r == QMessageBox::Yes) {
			db->dropDB();
		}
	}

		);

    m_showChartAction = new QAction("Графики");
    connect(m_showChartAction, &QAction::triggered,
            this, &BatteriesTableWindow::showChart);
    m_showChartAction->setDisabled(true);

    m_showDischargeStatsAction = new QAction("Статистика по глубине циклов");
    connect(m_showDischargeStatsAction, &QAction::triggered,
            this, &BatteriesTableWindow::showDischargeStats);
    m_showDischargeStatsAction->setDisabled(true);
}


void BatteriesTableWindow::createTable()
{
    m_table = new QTableWidget();
	m_table->setSelectionBehavior(QTableWidget::SelectRows);
    m_table->setColumnCount(13);
    m_table->setRowCount(1);
    m_table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    auto headers = QStringList() <<  "Клиент"
                                  << "ID батареи"
                                  << "Напряжение"
                                  << "Емкость" << "Состояние"
                                  << "Разрядка"
                                  << "Зарядка"
                                  << "В режиме ожидания"
                                  << "Всего отдано Ач"
                                  << "Срок службы"
                                  << "Отработано циклов"
                                  << "Ср. глубина цикла"
                                  << "Комментарий";
    m_table->setHorizontalHeaderLabels(headers);

    m_layout->addWidget(m_table, 0,0);

	connect(m_table, &QTableWidget::itemSelectionChanged,
			[this](){
		bool batterySelected = m_table->selectedItems().size() > 0;
		m_showCurDataAction->setDisabled(!batterySelected);
        m_showChartAction->setDisabled(!batterySelected);
        m_showDischargeStatsAction->setDisabled(!batterySelected);

	});

}

void BatteriesTableWindow::fillTable()
{

    m_table->clearContents();
    m_table->setRowCount(0);
    QMap<int, Battery*> batteries = db->batteries();
    foreach (auto b, batteries) {
        int rowId = m_table->rowCount();
        m_table->insertRow(rowId);

        m_table->setItem(rowId, 0, createWidgetItem(b->getClient()));
        m_table->setItem(rowId, 1, createWidgetItem(b->batteryId()));
        m_table->setItem(rowId, 2, createWidgetItem(b->getNomVoltage()));
        m_table->setItem(rowId, 3, createWidgetItem(b->getNomCapacity()));
        m_table->setItem(rowId, 4, createWidgetItem(
                             (b->activeDuringDay(QDate::currentDate())) ? "В работе" : "Выключена"));
        m_table->setItem(rowId, 5, createWidgetItem(QVariant::fromValue(b->dischargingDuration())));
        m_table->setItem(rowId, 6, createWidgetItem(QVariant::fromValue(b->chargingDuration())));
        m_table->setItem(rowId, 7, createWidgetItem(QVariant::fromValue(b->standbyDuration())));
        m_table->setItem(rowId, 8, createWidgetItem(b->totalOutcome()));
        m_table->setItem(rowId, 9, createWidgetItem(b->monthsOld()));
        m_table->setItem(rowId, 10, createWidgetItem(b->cyclesTotal()));
        m_table->setItem(rowId, 11, createWidgetItem(b->avgCycleDepth()));
        m_table->setItem(rowId, 12, createWidgetItem(b->getComment()));


    }

}

void BatteriesTableWindow::showCurData()
{
	auto items = m_table->selectedItems();
	if (items.size()) {
	int id = items[1]->data(Qt::DisplayRole).toInt();
	app->showCurData(id);
	}


}

void BatteriesTableWindow::showChart()
{
    auto items = m_table->selectedItems();
    if (items.size()) {
    int id = items[1]->data(Qt::DisplayRole).toInt();
    app->showChart(id);
    }


}

void BatteriesTableWindow::showDischargeStats()
{
    auto items = m_table->selectedItems();
    if (items.size()) {
    int id = items[1]->data(Qt::DisplayRole).toInt();
    app->showDischargeStats(id);
    }

}


QTableWidgetItem* BatteriesTableWindow::createWidgetItem(QVariant v)
{
    auto item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, v);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    return item;

}

}
