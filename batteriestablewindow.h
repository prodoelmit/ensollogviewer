#ifndef BATTERIESTABLEWINDOW_H
#define BATTERIESTABLEWINDOW_H
#include <QToolBar>
#include <QWidget>
#include <QGridLayout>
#include <QTableWidget>
#include <bsoncxx/types.hpp>
#include <iostream>
#include <bsoncxx/builder/stream/document.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/pipeline.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/document/element.hpp>
#include <bsoncxx/types/value.hpp>
#include <QMainWindow>
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;


namespace logviewer {
class BatteriesTableWindow: public QMainWindow
{
	Q_OBJECT
public:
    BatteriesTableWindow();
    void initLayout();
	void initMenu();

	void initActions();
    void createTable();
    void fillTable();

public slots:
	void showCurData();
    void showChart();
    void showDischargeStats();
private:
    QTableWidgetItem *createWidgetItem(QVariant v);
private:
	QWidget* m_cWidget;
    QGridLayout* m_layout;
    QTableWidget* m_table;
	QToolBar* m_batteryToolbar;
	QToolBar* m_nilToolbar;


	QAction* m_showCurDataAction;
	QAction* m_importCurDataAction;
	QAction* m_importLifeDataAction;
	QAction* m_dropMongoAction;
    QAction* m_showChartAction;
    QAction* m_showDischargeStatsAction;


};
}

#endif // BATTERIESTABLEWINDOW_H
