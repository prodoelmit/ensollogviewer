#include "battery.h"
#include <mongocxx/database.hpp>
#include "qdebug.h"
#include <mongocxx/stdx.hpp>
#include <bsoncxx/json.hpp>
#include <QMap>
//#include "lvapp.h"

#include <mongocxx/pipeline.hpp>
using mongocxx::database;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::document::element;

namespace logviewer {
Battery::Battery(bsoncxx::document::view el, mongocxx::database db)
    : m_db(db)
{

    qDebug() << QString::fromStdString(bsoncxx::to_json(el));
    m_client = QString::fromStdString(el["client"].get_utf8().value.to_string());
    m_batteryId = el["batteryId"].get_int32().value;
    m_nomVoltage = el["nomVoltage"].get_int32().value; m_nomCapacity = el["nomCapacity"].get_int32().value;

    m_comment = QString::fromStdString(el["comment"].get_utf8().value.to_string());


    getAdditionalInfo(db);

}

int Battery::batteryId()
{
    return m_batteryId;

}

Duration Battery::dischargingDuration()
{
    return m_dischargingDuration;

}

Duration Battery::chargingDuration()
{
    return m_chargingDuration;

}

Duration Battery::standbyDuration()
{
    return m_standbyDuration;

}

bool Battery::state()
{
    return m_state;

}

quint32 Battery::totalOutcome()
{
    return m_totalOutcome;

}

int Battery::monthsOld()
{
    return m_monthsOld;

}

quint32 Battery::cyclesTotal()
{
    return m_cyclesTotal;

}

quint8 Battery::avgCycleDepth()
{
    return m_avgCycleDepth;

}

bool Battery::activeDuringDay(QDate d)
{

    auto filter = document{} << "batteryId" << m_batteryId
                             << "state.code" << (int)Enums::Discharging << finalize;

    auto cursor = m_db["curData"].find(filter.view());


    bool active = false;
    for (auto&& doc : cursor) {
        bsoncxx::types::b_date startDate = doc["dates"]["start"].get_date();
        bsoncxx::types::b_date stopDate = doc["dates"]["stop"].get_date();

        if (QDateTime::fromMSecsSinceEpoch(startDate.to_int64()).date() == d ||
        QDateTime::fromMSecsSinceEpoch(stopDate.to_int64()).date() == d
        ){
            active = true;
            break;

        }
    }
    return active;

}

void Battery::getAdditionalInfo(mongocxx::database db)
{

    //get durations

    mongocxx::pipeline stages;
    bsoncxx::builder::stream::document group_stage;
    bsoncxx::builder::stream::document match_stage;

    match_stage << "batteryId" << m_batteryId;

    group_stage << "_id" << "$state.code" << "total" << open_document
                << "$sum"
                << "$dates.duration"
                << close_document;

    stages.match(match_stage.view());
    stages.group(group_stage.view());

    auto cursor = db["curData"].aggregate(stages);



    QMap<Enums::State, Duration> stateDurations;
    for (auto&& doc : cursor) {
        auto state = doc["_id"].get_int32().value;
        auto duration = doc["total"].get_int32();

        stateDurations.insert((Enums::State)(state), Duration(duration));
    }

    m_chargingDuration = stateDurations[Enums::Charging];
    m_dischargingDuration = stateDurations[Enums::Discharging];
    m_standbyDuration = stateDurations[Enums::Standby];


    calculateLifeDataProps(db);
    calculateCurDataProps(db);


}

void Battery::calculateLifeDataProps(mongocxx::database db)
{

	// <Calculate age>
	mongocxx::pipeline stages;
	bsoncxx::builder::stream::document matchStage;
	bsoncxx::builder::stream::document sortStage;
	bsoncxx::builder::stream::document groupStage;
	bsoncxx::builder::stream::document projectStage;


	matchStage << "batteryId" << m_batteryId;

	sortStage << "monthsSinceEpoch" << 1;

	groupStage << "_id" << "id"
			   << "lastMonth" << open_document
			   << "$last" << "$monthsSinceEpoch"
			   << close_document
			   << "firstMonth" << open_document
			   << "$first" << "$monthsSinceEpoch"
			   << close_document
			 << "totalOutcome" << open_document
			 << "$last" << "$energy.total"
			 << close_document
			 << "totalCycles" << open_document
			 << "$last" << "$cycles.total"
			 << close_document
			 << "avgCycleDepth" << open_document
			 << "$last" << "$cycleDepth.total"
			 << close_document;

	projectStage << "monthsDiff" << open_document
				 << "$subtract" << open_array
				 << "$lastMonth"
				 << "$firstMonth"
				 << close_array
				 << close_document
				 << "totalOutcome" << 1
				 << "totalCycles" << 1
				 << "avgCycleDepth" << 1;

	stages.match(matchStage.view());
	stages.sort(sortStage.view());
	stages.group(groupStage.view());
	stages.project(projectStage.view());


    auto cursor = db["lifeData"].aggregate(stages);



	for (bsoncxx::document::view doc : cursor) {
		int months = doc["monthsDiff"].get_int32().value;
		m_monthsOld = months;

		int cyclesTotal = doc["totalCycles"].get_int32().value;
		m_cyclesTotal = cyclesTotal;

		m_avgCycleDepth = doc["avgCycleDepth"].get_int32().value;

		m_totalOutcome = doc["totalOutcome"].get_int32().value;
		qDebug() << months << m_cyclesTotal << m_avgCycleDepth << m_totalOutcome;
		break;

	}


	// </Calculate age>






}

void Battery::calculateCurDataProps(mongocxx::database db)
{

    mongocxx::options::find projOptions{};
    projOptions.projection(document{} << "charge.start" << 1 << "charge.stop" << 1 << "dates.duration" << 1 << finalize);

    auto filter = document{} << "batteryId" << m_batteryId
                             << "state.code" << 0x03 << finalize;

    auto cursor = db["curData"].find(filter.view(), projOptions);



    bool first = true;
    int prevEndCharge = 0;
    int curStartCharge = 0;
    int curEndCharge = 0;
    for (bsoncxx::document::view doc : cursor) {

//        qDebug() << QString::fromStdString(bsoncxx::to_json(doc));
        int startCharge = doc["charge"]["start"].get_int32().value;
        int endCharge = doc["charge"]["stop"].get_int32().value;
        int duration = doc["dates"]["duration"].get_int32().value;
        if (!first) {
            if (duration >= 60 * 1000) {
                prevEndCharge = curEndCharge;
                curStartCharge = startCharge;
                curEndCharge = endCharge;

                int diff = prevEndCharge - curStartCharge; // it should be positive
                m_dischargingDepths << diff;
            }
        } else {
            first = false;
            curEndCharge = endCharge;
        }



    }

    qreal avg = 0;
    qreal cyclesCount = m_dischargingDepths.length();
    foreach (int a, m_dischargingDepths) {
        avg += a / cyclesCount;
    }


//    m_avgCycleDepth = avg;



}

QString Battery::getComment() const
{
    return m_comment;
}

qint16 Battery::getNomCapacity() const
{
    return m_nomCapacity;
}

qint16 Battery::getNomVoltage() const
{
    return m_nomVoltage;
}

quint16 Battery::getBatteryId() const
{
    return m_batteryId;
}

QString Battery::getClient() const
{
    return m_client;
}
}
