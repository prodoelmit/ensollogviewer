#ifndef BATTERY_H
#define BATTERY_H
#include "duration.h"
#include <QDate>
#include <bsoncxx/document/element.hpp>
#include <QString>
#include "enums.h"
#include <mongocxx/database.hpp>
#include <QVector>


namespace logviewer {
class Battery
{
public:
    Battery(bsoncxx::document::view el, mongocxx::database db);
    int batteryId();
    Duration dischargingDuration();
    Duration chargingDuration();
    Duration standbyDuration();
    bool state();
    quint32 totalOutcome();
    int monthsOld();
    quint32 cyclesTotal();

    quint8 avgCycleDepth();

    bool activeDuringDay(QDate d);



    QString getClient() const;

    quint16 getBatteryId() const;

    qint16 getNomVoltage() const;

    qint16 getNomCapacity() const;

    QString getComment() const;

private:
    void getAdditionalInfo(mongocxx::database db);
    void calculateLifeDataProps(mongocxx::database db);
    void calculateCurDataProps(mongocxx::database db);



private:

    QString m_client;
    quint16 m_batteryId;
    qint16 m_nomVoltage;
    qint16 m_nomCapacity;
    bool m_state; // true if had any discharge during last 24 hours;
    QString m_comment;

    Duration m_chargingDuration;
    Duration m_dischargingDuration;
    Duration m_standbyDuration;

    QVector<int> m_dischargingDepths;
    int m_avgCycleDepth;
    int m_cyclesTotal;
    int m_totalOutcome;
    int m_monthsOld;


    mongocxx::database m_db;




};
}

#endif // BATTERY_H
