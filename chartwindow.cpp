#include "chartwindow.h"
#include "lvapp.h"
#include "curdatum.h"
#include "qdebug.h"

namespace logviewer{
ChartWindow::ChartWindow(int batteryId)
    : QWidget()
    , m_batteryId(batteryId)
    , m_series(0)
    , m_xAxis(0)
    , m_yAxis(0)
{
    setWindowTitle("Графики характеристик");
    m_field = Enums::StartCharge;

    m_layout = new QGridLayout;
    setLayout(m_layout);

    m_chartView = new QChartView(this);

    QLabel* fromLabel = new QLabel("Начало интервала");
    QLabel* toLabel = new QLabel("Начало интервала");
    m_fromDateEdit = new QDateTimeEdit(QDateTime::fromMSecsSinceEpoch(0));
    m_fromDateEdit->setDisplayFormat("dd.MM.yyyy hh:mm");
    m_fromDateEdit->setCalendarPopup(true);
    m_toDateEdit = new QDateTimeEdit(QDateTime::currentDateTime());
    m_toDateEdit->setDisplayFormat("dd.MM.yyyy hh:mm");
    m_toDateEdit->setCalendarPopup(true);

    m_fieldCombobox = new QComboBox();
    m_fieldCombobox->addItem("Заряд", (int)Enums::StartCharge);
    m_fieldCombobox->addItem("Напряжение", (int)Enums::StartVoltage);
    m_fieldCombobox->addItem("Степень разряда", (int)Enums::StartDischargeDegree);
    m_fieldCombobox->setCurrentIndex(0);

    connect( m_fieldCombobox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
             this, &ChartWindow::updateSeries);

    connect(m_fromDateEdit, &QDateTimeEdit::dateTimeChanged,
            this, &ChartWindow::updateSeries);
    connect(m_toDateEdit, &QDateTimeEdit::dateTimeChanged,
            this, &ChartWindow::updateSeries);

    m_layout->addWidget(m_chartView, 0, 0, 1, 3);
    m_layout->addWidget(fromLabel, 1, 0);
    m_layout->addWidget(toLabel, 1, 1);
    m_layout->addWidget(m_fromDateEdit,2,0);
    m_layout->addWidget(m_toDateEdit,2,1);
    m_layout->addWidget(m_fieldCombobox, 2, 2, 1, 1);


//    m_chartView->chart()->setAxisX(m_xAxis, m_series);
//    m_chartView->chart()->setAxisY(m_yAxis, m_series);
//    m_chartView->chart()->addSeries(m_series);
    updateSeries();

}

void ChartWindow::updateSeries()
{
    auto f = (Enums::CurDatumField)(m_fieldCombobox->currentData(Qt::UserRole).toInt());
//    qDebug() << f;
    QLineSeries* series = new QLineSeries;
//    qDebug() << "CHART" << m_chartView->chart();
    QVector<CurDatum*> data = db->queryCurData(m_batteryId);
    qSort(data.begin(), data.end(), [](CurDatum* d1, CurDatum* d2) {
        return d1->startDatetime() < d2->startDatetime();
    });

    QDateTime from = m_fromDateEdit->dateTime();
    QDateTime to = m_toDateEdit->dateTime();
    qDebug() << from << to;
    foreach (CurDatum* d, data) {
        if (d->startDatetime() < from || d->startDatetime() > to) continue;
        auto x = d->startDatetime().toMSecsSinceEpoch();
        switch (f) {
        case Enums::StartCharge:
            series->append(x, d->startCharge());
            break;
        case Enums::StartVoltage:
            series->append(x, d->startVoltage());
            break;
        case Enums::StartDischargeDegree:
            series->append(x, d->startDischargeDegree());
            break;
        default:
            break;
        }

    }
    if (m_xAxis) {
    m_chartView->chart()->removeAxis(m_xAxis);
    delete m_xAxis;
    }
    if (m_yAxis) {
    m_chartView->chart()->removeAxis(m_yAxis);
    delete m_yAxis;
    }
    m_chartView->chart()->removeAllSeries();


    m_xAxis = new QDateTimeAxis(m_chartView->chart());
    m_xAxis->setVisible(true);
    m_yAxis = new QValueAxis(m_chartView->chart());
    m_yAxis->setVisible(true);

    m_chartView->chart()->addSeries(series);
    series->attachAxis(m_xAxis);
    series->setPointsVisible(true);
    series->attachAxis(m_yAxis);
    m_chartView->chart()->setAxisX(m_xAxis, series);
    m_chartView->chart()->setAxisY(m_yAxis, series);
//    m_chartView->chart()->createDefaultAxes();
    if (f == Enums::StartCharge)  m_yAxis->setRange(0, 100);
//    m_chartView->chart()->axisY()->setRange(0, 100);
    m_series = series;
    series->setName(m_fieldCombobox->currentText());
//    m_series->show();

}

}
