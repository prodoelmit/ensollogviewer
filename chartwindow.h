#ifndef CHARTWINDOW_H
#define CHARTWINDOW_H
#include <QComboBox>
#include <QWidget>
#include <QtCharts/QChart>
#include "enums.h"
#include <QDateTimeEdit>
#include <QGridLayout>
#include <QtCharts/QChartView>
#include <QLineSeries>
#include <QDateTimeAxis>
#include <QValueAxis>
using namespace QtCharts;


namespace logviewer {
class ChartWindow: public QWidget
{
public:
    ChartWindow(int batteryId);

public slots:
    void updateSeries();
private:

    QGridLayout* m_layout;
    Enums::CurDatumField m_field;

    QComboBox* m_fieldCombobox;

    QChartView* m_chartView;

    QDateTime* m_fromDate;
    QDateTime* m_toDate;

    QDateTimeEdit* m_fromDateEdit;
    QDateTimeEdit* m_toDateEdit;
    QLineSeries* m_series;

    QValueAxis* m_yAxis;
    QDateTimeAxis* m_xAxis;

    int m_batteryId;

};

}
#endif // CHARTWINDOW_H
