#include "columnselectorwidget.h"
#include <QLabel>

namespace logviewer {

ColumnSelectorWidget::ColumnSelectorWidget(QList<Enums::CurDatumField> fields)
    : QWidget()
    , m_allFields(fields)
    , m_enabledFields(fields.toSet())
{
    m_layout = new QGridLayout();
    setLayout(m_layout);
    m_enabledFieldsList = new QListWidget();
    m_enabledFieldsList->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

    m_disabledFieldsList = new QListWidget();
//    m_disabledFieldsList->setMaximumHeight(1500);
//    m_disabledFieldsList->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum));
    m_enabledFieldsList->setSelectionMode(QListWidget::ExtendedSelection);
    m_disabledFieldsList->setSelectionMode(QListWidget::ExtendedSelection);



    QLabel* m_enabledLabel = new QLabel("Отображаемые столбцы");
    QLabel* m_disabledLabel = new QLabel("Скрытые столбцы");
    m_enableButton = new QPushButton(">");
    m_disableButton = new QPushButton("<");

    m_layout->setRowStretch(0, 0);
    m_layout->setRowStretch(1, 1);
    m_layout->setRowStretch(2, 1);
    m_layout->setRowStretch(3, 1);

    m_layout->addWidget(m_disabledLabel, 0, 0, 1, 1);
    m_layout->addWidget(m_disabledFieldsList, 1, 0, 3, 1);
    m_layout->addWidget(m_enableButton, 1, 1, 1, 1);
    m_layout->addWidget(m_disableButton, 3, 1, 1, 1);
    m_layout->addWidget(m_enabledLabel, 0, 2, 1, 1);
    m_layout->addWidget(m_enabledFieldsList, 1, 2, 3, 1);

    connect(m_enableButton, &QPushButton::clicked,
            this, &ColumnSelectorWidget::enableSelectedFields);
    connect(m_disableButton, &QPushButton::clicked,
            this, &ColumnSelectorWidget::disableSelectedFields);

    refresh();
}


void ColumnSelectorWidget::enableSelectedFields()
{
    auto items = m_disabledFieldsList->selectedItems();
    foreach (auto item, items) {
        auto e = (Enums::CurDatumField)(item->data(Qt::UserRole).toInt());
        enableField(e);
    }
    refresh();
    emitColumnsChanged();


}

void ColumnSelectorWidget::disableSelectedFields()
{
    auto items = m_enabledFieldsList->selectedItems();
    foreach (auto item, items) {
        auto e = (Enums::CurDatumField)(item->data(Qt::UserRole).toInt());
        disableField(e);
    }
    refresh();
    emitColumnsChanged();

}

void ColumnSelectorWidget::enableField(Enums::CurDatumField f)
{
    m_enabledFields.insert(f);

}

void ColumnSelectorWidget::disableField(Enums::CurDatumField f)
{
    m_enabledFields.remove(f);

}

void ColumnSelectorWidget::refresh()
{
    m_enabledFieldsList->clear();
    m_disabledFieldsList->clear();
    foreach (auto e, m_allFields) {
        QListWidgetItem* it = new QListWidgetItem(Enums::toString(e));
        it->setData(Qt::UserRole, e);
        if (m_enabledFields.contains(e)) {
            m_enabledFieldsList->addItem(it);
        } else {
            m_disabledFieldsList->addItem(it);
        }
    }

}

void ColumnSelectorWidget::emitColumnsChanged()
{
    emit columnsChanged(m_enabledFields);
}

}
