#ifndef COLUMNSELECTORWIDGET_H
#define COLUMNSELECTORWIDGET_H
#include "enums.h"
#include <QSet>
#include <QGridLayout>
#include <QListWidget>
#include <QPushButton>

namespace logviewer {
class ColumnSelectorWidget: public QWidget
{
    Q_OBJECT
public:
    ColumnSelectorWidget(QList<Enums::CurDatumField> fields);
public slots:
    void enableSelectedFields();
    void disableSelectedFields();
    void enableField(Enums::CurDatumField f);
    void disableField(Enums::CurDatumField f);
    void refresh();
signals:
    void columnsChanged(QSet<Enums::CurDatumField> enabledColumns);

private:
    void emitColumnsChanged();

    QListWidget* m_disabledFieldsList;
    QListWidget* m_enabledFieldsList;
    QGridLayout* m_layout;
    QList<Enums::CurDatumField> m_allFields;
    QSet<Enums::CurDatumField> m_enabledFields;

    QPushButton* m_enableButton;
    QPushButton* m_disableButton;
};
}

#endif // COLUMNSELECTORWIDGET_H
