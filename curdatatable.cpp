#include "curdatatable.h"
#include <QRgb>
#include <QHeaderView>
#include <QFileDialog>
#include <QShortcut>
#include <QVector>
#include "qdebug.h"
#include "duration.h"
#include "xlsxdocument.h"

namespace logviewer {

CurDataTable::CurDataTable()
    : QTableWidget()
    , m_minMaxModeEnabled(false)
{

    init();
}

void CurDataTable::init()
{
    setColumnCount(cCount());

    horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    QStringList headers;
    headers << "Состояние"
            << "Начало"
            << "Конец"
            << "Длительность"
            << "Начальный заряд"
            << "Конечный заряд"
            << "Разность зарядов"
            << "Начальное напряжение"
            << "Конечное напряжение"
            << "Разность напряжений"
            << "Начальная степень разряда"
            << "Конечная степень разряда"
            << "Разность степени разряда"
            << "Емкость"
            << "Эффективная емкость"
            << "Ячейка с макс.температурой"
            << "Макс. температура"
            << "Ток"
            << "Ячейка с минимальным напряжением"
            << "Минимальное напряжение"
            << "Ячейка с максимальным напряжением"
            << "Максимальное напряжение"
            << "Напряжение в ячейке 1"
            << "Напряжение в ячейке 2"
            << "Напряжение в ячейке 3"
            << "Напряжение в ячейке 4"
            << "Напряжение в ячейке 5"
            << "Напряжение в ячейке 6"
            << "Напряжение в ячейке 7"
            << "Напряжение в ячейке 8"
            << "Напряжение в ячейке 9"
            << "Напряжение в ячейке 10"
            << "Напряжение в ячейке 11"
            << "Напряжение в ячейке 12"
            << "Напряжение в ячейке 13"
            << "Напряжение в ячейке 14"
            << "Напряжение в ячейке 15"
            << "Напряжение в ячейке 16"
            << "Мин.температура логгера"
            << "Макс.температура логгера"
            << "Ошибки"
               ;

    setHorizontalHeaderLabels(headers);


    columnRoles.insert(Enums::StateCode, 0);
    columnRoles.insert(Enums::StartDatetime, 1);
    columnRoles.insert(Enums::StopDatetime, 2);
    columnRoles.insert(Enums::Duration, 3);
    columnRoles.insert(Enums::StartCharge, 4);
    columnRoles.insert(Enums::EndCharge, 5);
    columnRoles.insert(Enums::DiffCharge, 6);
    columnRoles.insert(Enums::StartVoltage, 7);
    columnRoles.insert(Enums::EndVoltage, 8);
    columnRoles.insert(Enums::DiffVoltage, 9);
    columnRoles.insert(Enums::StartDischargeDegree, 10);
    columnRoles.insert(Enums::EndDischargeDegree, 11);
    columnRoles.insert(Enums::DiffDischargeDegree, 12);
    columnRoles.insert(Enums::MinCapacity, 13);
    columnRoles.insert(Enums::MinEffCapacity, 14);
    columnRoles.insert(Enums::MaxTempCellId, 15);
    columnRoles.insert(Enums::MaxTemp, 16);
    columnRoles.insert(Enums::MaxCurrent, 17);
    columnRoles.insert(Enums::MinVoltageCellId, 18);
    columnRoles.insert(Enums::MinVoltage, 19);
    columnRoles.insert(Enums::MaxVoltageCellId, 20);
    columnRoles.insert(Enums::MaxVoltage, 21);
    columnRoles.insert(Enums::CellVoltage1, 22);
    columnRoles.insert(Enums::CellVoltage2, 23);
    columnRoles.insert(Enums::CellVoltage3, 24);
    columnRoles.insert(Enums::CellVoltage4, 25);
    columnRoles.insert(Enums::CellVoltage5, 27);
    columnRoles.insert(Enums::CellVoltage6, 27);
    columnRoles.insert(Enums::CellVoltage7, 28);
    columnRoles.insert(Enums::CellVoltage8, 29);
    columnRoles.insert(Enums::CellVoltage9, 30);
    columnRoles.insert(Enums::CellVoltage10, 31);
    columnRoles.insert(Enums::CellVoltage11, 32);
    columnRoles.insert(Enums::CellVoltage12, 33);
    columnRoles.insert(Enums::CellVoltage13, 34);
    columnRoles.insert(Enums::CellVoltage14, 35);
    columnRoles.insert(Enums::CellVoltage15, 36);
    columnRoles.insert(Enums::CellVoltage16, 37);
    columnRoles.insert(Enums::MinLoggerTemp, 38);
    columnRoles.insert(Enums::MaxLoggerTemp, 39);
    columnRoles.insert(Enums::ErrorCode, 40);


    QShortcut* shortcut = new QShortcut(QKeySequence("Ctrl+E"), this);
    connect( shortcut, &QShortcut::activated,
             this, &CurDataTable::exportToXlsx);


}


void CurDataTable::fillWithData(Database *db)
{
    auto data = db->curData();
    for (QVector<CurDatum*>::iterator it = data->begin(); it != data->end(); ++it) {
        appendRow(*it);
    }

}

void CurDataTable::fillWithData(QVector<CurDatum*> data)
{
	for (QVector<CurDatum*>::iterator it = data.begin(); it != data.end(); ++it) {
		appendRow(*it);
	}
}

void CurDataTable::appendRow(CurDatum *d)
{
    int rowId = this->rowCount();
    this->insertRow(rowId);

        bool t2 = d->isOfType2();

        auto stateItem = new QTableWidgetItem(d->stateString());
        QVariantList stateList;
        stateList << d->state();
        stateItem->setData(Qt::UserRole, stateList);
        stateItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        this->setItem(rowId, 0, stateItem);

        this->setItem(rowId, 1, createWidgetItem(d->startDatetime()));

        if (t2) {
            this->setItem(rowId, 2, createWidgetItem(d->stopDatetime()));
            this->setItem(rowId, 3, createWidgetItem(QVariant::fromValue(d->duration())));
        }
        this->setItem(rowId, 4, createWidgetItem(d->m_startCharge));
        if (t2) {
        this->setItem(rowId, 5, createWidgetItem(d->m_endCharge));
        this->setItem(rowId, 6, createWidgetItem(d->m_diffCharge));
        }
        this->setItem(rowId, 7, createWidgetItem(d->m_startVoltage));
        if (t2) {
        this->setItem(rowId, 8, createWidgetItem(d->m_endVoltage));
        this->setItem(rowId, 9, createWidgetItem(d->m_diffVoltage));
        }
        this->setItem(rowId, 10, createWidgetItem(d->m_startDischargeDegree));
        if (t2) {
        this->setItem(rowId, 11, createWidgetItem(d->m_endDischargeDegree));
        this->setItem(rowId, 12, createWidgetItem(d->m_diffDischargeDegree));
        this->setItem(rowId, 13, createWidgetItem(d->m_curCap));
        this->setItem(rowId, 14, createWidgetItem(d->m_effCap));
        } else {
            this->setItem(rowId, 13, createWidgetItem(d->m_minCap));
            this->setItem(rowId, 14, createWidgetItem(d->m_minEffCap));
        }

        this->setItem(rowId, 15, createWidgetItem(d->m_maxTempCellId));
        this->setItem(rowId, 16, createWidgetItem(d->m_maxTemp));
        if (t2) {
        this->setItem(rowId, 17, createWidgetItem(d->m_maxCurrent));
        } else {
            this->setItem(rowId, 17, createWidgetItem(d->m_current));
        }
        this->setItem(rowId, 18, createWidgetItem(d->m_minVoltageCellId));
        this->setItem(rowId, 19, createWidgetItem(d->m_minVoltage));
        this->setItem(rowId, 20, createWidgetItem(d->m_maxVoltageCellId));
        this->setItem(rowId, 21, createWidgetItem(d->m_maxVoltage));

        this->setItem(rowId, 22, createWidgetItem(d->m_cellVoltage[0]));
        this->setItem(rowId, 23, createWidgetItem(d->m_cellVoltage[1]));
        this->setItem(rowId, 24, createWidgetItem(d->m_cellVoltage[2]));
        this->setItem(rowId, 25, createWidgetItem(d->m_cellVoltage[3]));
        this->setItem(rowId, 26, createWidgetItem(d->m_cellVoltage[4]));
        this->setItem(rowId, 27, createWidgetItem(d->m_cellVoltage[5]));
        this->setItem(rowId, 28, createWidgetItem(d->m_cellVoltage[6]));
        this->setItem(rowId, 29, createWidgetItem(d->m_cellVoltage[7]));
        this->setItem(rowId, 30, createWidgetItem(d->m_cellVoltage[8]));
        this->setItem(rowId, 31, createWidgetItem(d->m_cellVoltage[9]));
        this->setItem(rowId, 32, createWidgetItem(d->m_cellVoltage[10]));
        this->setItem(rowId, 33, createWidgetItem(d->m_cellVoltage[11]));
        this->setItem(rowId, 34, createWidgetItem(d->m_cellVoltage[12]));
        this->setItem(rowId, 35, createWidgetItem(d->m_cellVoltage[13]));
        this->setItem(rowId, 36, createWidgetItem(d->m_cellVoltage[14]));
        this->setItem(rowId, 37, createWidgetItem(d->m_cellVoltage[15]));



        this->setItem(rowId, 38, createWidgetItem(d->m_loggerTemp1));
        this->setItem(rowId, 39, createWidgetItem(d->m_loggerTemp2));

        auto errorItem = new QTableWidgetItem(d->errorString());
        errorItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        auto errors = d->errors();
        QVariantList errorsList;
        foreach (auto e, errors) {
            errorsList << e;
        }


        errorItem->setData(Qt::UserRole, errorsList);
        this->setItem(rowId, 40, errorItem);




}

void CurDataTable::setVisibleColumns(QSet<Enums::CurDatumField> enums)
{
    QSet<int> numbers;
    foreach (auto e, enums) {
        numbers.insert(columnRoles[e]);
    }
    setVisibleColumns(numbers);

}

void CurDataTable::setVisibleColumns(QSet<int> numbers)
{
    for (int i = 0; i < cCount(); i++) {
        this->setColumnHidden(i, !numbers.contains(i));
    }

    qDebug() << this->columnCount();

}

void CurDataTable::clearFilters()
{
    m_minMaxModeEnabled = false;
    m_minRows.clear();
    m_maxRows.clear();
    for (int i = 0; i < this->rowCount(); i++) {
        setRowHidden(i, false);
    }
    colorRows();
}

void CurDataTable::applyFilter(Enums::CurDatumField f, int s, QVariant v1, QVariant v2)
{
    auto t = Enums::type(f);
    int col = columnRoles[f];
    switch (t) {
    case Enums::NumberType:
        switch ((Enums::NumberRelationships)(s)) {
        case Enums::EQ:
            filterNumberEq(col, v1.toInt());
            break;
        case Enums::Greater:
            filterNumberGreaterThan(col, v1.toInt());
            break;
        case Enums::Less:
            filterNumberLessThan(col, v2.toInt());
            break;
        case Enums::NEQ:
            filterNumberNeq(col, v1.toInt());
            break;
        case Enums::NumberMinMax:
            filterMinMax<int>(col);
            break;
        default:
            break;
        }
        break;
    case Enums::DateType:
        switch ((Enums::DateRelationships)(s)) {
        case Enums::Later:
            filterDateLater(col, v1.toDateTime());
            break;
        case Enums::Earlier:
            filterDateEarlier(col, v2.toDateTime());
            break;
        case Enums::Between:
            filterDateBetween(col, v1.toDateTime(), v2.toDateTime());
            break;
        case Enums::DateMinMax:
            filterMinMax<QDateTime>(col);
            break;
        default:
            break;
        }
        break;
    case Enums::DurationType:
        switch ((Enums::TimeRelationships)(s)) {
        case Enums::Longer:
            filterTimeLongerThan(col, v1.value<Duration>());
            break;
        case Enums::Shorter:
            filterTimeShorterThan(col, v2.value<Duration>());
            break;
        case Enums::TimeBetween:
            filterTimeBetween(col, v1.value<Duration>(), v2.value<Duration>());
            break;
        case Enums::TimeMinMax:
            filterMinMax<Duration>(col);
            break;
        default:
            break;
        }
        break;
    case Enums::EnumType:
        switch ((Enums::EnumRelationships)(s)) {
        case Enums::EnumAmong:
            filterEnumAmong(col, v1.toList(), f);
            break;
        case Enums::EnumNotAmong:
            filterEnumNotAmong(col, v1.toList(), f);
            break;
        case Enums::EnumEquals:
            filterEnumEquals(col, v1.toList(), f);
            break;
        }

    }


}

void CurDataTable::setMinMaxMode(bool enabled)
{

}

void CurDataTable::exportToXlsx()
{
    QString filename = QFileDialog::getSaveFileName();

    if (filename.length())  {
        int colI = 1;
        int rowI = 1;
        QXlsx::Document xlsx;
        for (int row = 0; row < this->rowCount(); row++) {
            if (isRowHidden(row)) continue;
            colI = 1;
            for (int col = 0; col < this->columnCount(); col++) {
                if (isColumnHidden(col)) continue;
                auto item = this->item(row, col);
                if (item) {
                    xlsx.write(rowI + 1, colI , this->item(row, col)->data(Qt::DisplayRole));
                }
                colI++;
            }
            rowI++;
        }

        colI	 = 1;
        rowI = 1;
        for (int col = 0; col < this->columnCount(); col++) {
            if (isColumnHidden(col)) continue;
            xlsx.write(1, colI , this->horizontalHeaderItem(col)->data(Qt::DisplayRole));
            colI++;
        }
        xlsx.saveAs(filename);
    }

}

QTableWidgetItem* CurDataTable::createWidgetItem(QVariant v)
{
    auto item = new QTableWidgetItem();
    item->setData(Qt::DisplayRole, v);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    return item;

}

void CurDataTable::filterNumberEq(int colId, int v)
{
    qDebug() << "Eq";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<int>()) {
            int q = itemValue.toInt();
            setRowHidden(i, q != v);
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterNumberNeq(int colId, int v)
{
    qDebug() << "Neq";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<int>()) {
            int q = itemValue.toInt();
            setRowHidden(i, q == v);
        } else {
           setRowHidden(i, true);
        }
    }
}

void CurDataTable::filterNumberLessThan(int colId, int v)
{
    qDebug() << "Less";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<int>()) {
            int q = itemValue.toInt();
            setRowHidden(i, q > v);
        } else {
           setRowHidden(i, true);
        }
    }
}

void CurDataTable::filterNumberGreaterThan(int colId, int v)
{
    qDebug() << "Greater";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<int>()) {
            int q = itemValue.toInt();
            setRowHidden(i, q < v);
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterDateLater(int colId, QDateTime v)
{
    qDebug() << "Later";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<QDateTime>()) {
            QDateTime q = itemValue.toDateTime();
            setRowHidden(i, !(q >= v));
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterDateEarlier(int colId, QDateTime v)
{
    qDebug() << "Earlier";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<QDateTime>()) {
            QDateTime q = itemValue.toDateTime();
            setRowHidden(i, !(q <= v));
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterDateBetween(int colId, QDateTime v1, QDateTime v2)
{
    qDebug() << "Between";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<QDateTime>()) {
            QDateTime q = itemValue.toDateTime();
            setRowHidden(i, !(q >= v1 && q <= v2));
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterTimeLongerThan(int colId, Duration v)
{
    qDebug() << "Longer";
    qDebug() << v.secs();
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<Duration>()) {
            Duration q = itemValue.value<Duration>();
            qDebug() << q.secs();
            setRowHidden(i, !(q > v));
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterTimeShorterThan(int colId, Duration v)
{
    qDebug() << "Shorter";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<Duration>()) {
            Duration q = itemValue.value<Duration>();
            setRowHidden(i, !(q < v));
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterTimeBetween(int colId, Duration v1, Duration v2)
{
    qDebug() << "TimeBetween";
    qDebug() << v1.toString() << v2.toString();
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::DisplayRole);
        if (itemValue.canConvert<Duration>()) {
            Duration q = itemValue.value<Duration>();
            qDebug() << q.toString();
            setRowHidden(i, !(q > v1 && q < v2));
        } else {
           setRowHidden(i, true);
        }
    }

}

void CurDataTable::filterEnumAmong(int colId, QVariantList v1, Enums::CurDatumField f)
{
    qDebug() << "EnumAmong";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::UserRole);
            QList<QVariant> q = itemValue.toList();
            QList<int> enums;
            foreach (auto a, q) {
                enums << a.toInt();
            }
            bool among = false;
            foreach (auto v, v1) {
                among = among || enums.contains(v.toInt());
            }

            setRowHidden(i, !among);


    }

}

void CurDataTable::filterEnumNotAmong(int colId, QVariantList v1, Enums::CurDatumField f)
{
    qDebug() << "EnumNotAmong";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::UserRole);
            QList<QVariant> q = itemValue.toList();
            QList<int> enums;
            foreach (auto a, q) {
                enums << a.toInt();
            }
            bool notAmong = true;
            foreach (auto v, v1) {
                notAmong = notAmong && !(enums.contains(v.toInt()));
            }

            setRowHidden(i, !notAmong);


    }

}

void CurDataTable::filterEnumEquals(int colId, QVariantList v1, Enums::CurDatumField f)
{
    qDebug() << "EnumNotAmong";
    for (int i = 0; i < this->rowCount(); i++) {
        if (isRowHidden(i)) {continue;}
        QTableWidgetItem* item = this->item(i, colId);
        if (!item) {setRowHidden(i, true); continue;}
        auto itemValue = item->data(Qt::UserRole);
            QList<QVariant> q = itemValue.toList();
            QList<int> enums;
            foreach (auto a, q) {
                enums << a.toInt();
            }
            bool equals = true;
            foreach (auto v, v1) {
                equals = equals && enums.contains(v.toInt());
            }

            equals = equals && (enums.size() == v1.size());


            setRowHidden(i, !equals);


    }

}




void CurDataTable::showMinMaxRows()
{
    if (!m_minMaxModeEnabled) return;
    for (int i = 0 ; i < this->rowCount(); i++) {
        setRowHidden(i, true);
    }

    for (auto minIt = m_minRows.begin(); minIt != m_minRows.end(); ++minIt) {
        QSet<int> columns = minIt.value();
        int row = minIt.key();
        setRowHidden(row, false);
        foreach (int col, columns) {
            item(row, col)->setBackgroundColor(QColor("#9FCADD"));
        }

    }
    for (auto maxIt = m_maxRows.begin(); maxIt != m_maxRows.end(); ++maxIt) {
        QSet<int> columns = maxIt.value();
        int row = maxIt.key();
        setRowHidden(row, false);
        foreach (int col, columns) {
            item(row, col)->setBackgroundColor(QColor("#DDB29F"));
        }

    }

}

void CurDataTable::colorRows()
{
    for (int row = 0; row < this->rowCount(); row++) {
        for (int col = 0; col < this->columnCount(); col++) {
            QTableWidgetItem* item = this->item(row, col);
            if (item) {
                 item->setBackgroundColor(QColor("white"));
            }


        }
    }

}

}
