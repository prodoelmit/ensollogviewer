#ifndef CURDATATABLE_H
#define CURDATATABLE_H
#include <QTableWidget>
#include "enums.h"
#include "curdatum.h"
#include "database.h"


namespace logviewer {
class CurDataTable: public QTableWidget
{
    Q_OBJECT
public:
    CurDataTable();
    void init();
    int cCount() const {return 41;}
	void fillWithData(Database* db);
	void fillWithData(QVector<CurDatum *> data);
    void appendRow(CurDatum* d);
    void setVisibleColumns(QSet<int> numbers);
    void clearFilters();
    void applyFilter(Enums::CurDatumField f, int s, QVariant v1, QVariant v2 );
    void setMinMaxMode(bool enabled);

    void showMinMaxRows();
    void colorRows();
public slots:
    void exportToXlsx();
    void setVisibleColumns(QSet<Enums::CurDatumField> enums);
private:
    QTableWidgetItem *createWidgetItem(QVariant v);
    void filterNumberEq(int colId, int v);
    void filterNumberNeq(int colId, int v);
    void filterNumberLessThan(int colId, int v);
    void filterNumberGreaterThan(int colId, int v);
    void filterDateLater(int colId, QDateTime v);
    void filterDateEarlier(int colId, QDateTime v);
    void filterDateBetween(int colId, QDateTime v1, QDateTime v2);

    void filterTimeLongerThan(int colId, Duration v);
    void filterTimeShorterThan(int colId, Duration v);
    void filterTimeBetween(int colId, Duration v1, Duration v2);
    void filterEnumAmong(int colId, QVariantList v1, Enums::CurDatumField f);
    void filterEnumNotAmong(int colId, QVariantList v1, Enums::CurDatumField f);
    void filterEnumEquals(int colId, QVariantList v1, Enums::CurDatumField f);

    template <typename T>
    void filterMinMax(int colId)
    {
        QSet<int> minRows;
        QSet<int> maxRows;
        T minValue;
        T maxValue;

        bool first = true;
        for (int i = 0; i < this->rowCount(); i++) {
            if (isRowHidden(i)) {continue;}
            QTableWidgetItem* item = this->item(i, colId);
            if (!item) {setRowHidden(i, true); continue;}
            auto itemValue = item->data(Qt::DisplayRole);
            if (!itemValue.canConvert<T>()) {
                continue;
            }
            T q = itemValue.value<T>();
            if (first) {
                minRows.clear();
                minRows << i;
                maxRows.clear();
                maxRows << i;
                minValue = q;
                maxValue = q;
                first = false;
            } else {
                if (q < minValue) {
                    minValue = q;
                    minRows.clear();
                    minRows << i;
                }  else if (q == minValue) {
                    minRows << i;
                };
                if (q == maxValue) {
                    maxRows << i;
                } else if (q > maxValue) {
                    maxValue = q;
                    maxRows.clear();
                    maxRows << i;
                }
            }

        }

        foreach (int i, minRows) {
            m_minRows[i].insert(colId);
        }
        foreach (int i, maxRows) {
            m_maxRows[i].insert(colId);
        }

        m_minMaxModeEnabled = true;



    }
private:
    QMap<Enums::CurDatumField, int> columnRoles;
    QMap<int, QSet<int>> m_maxRows;
    QMap<int, QSet<int>> m_minRows;
    bool m_minMaxModeEnabled;

};

}
#endif // CURDATATABLE_H
