#include "curdatum.h"
#include <bsoncxx/array/view.hpp>
#include "qdebug.h"
#include <chrono>
#include <bsoncxx/json.hpp>
#include <bsoncxx/document/element.hpp>
#include <bsoncxx/document/view.hpp>
#define BTIME(_time) bsoncxx::types::b_date(std::chrono::milliseconds(_time))
using bsoncxx::document::element;
using bsoncxx::array::view;


namespace logviewer{

CurDatum::CurDatum(QDataStream &in)
{
    in >> m_batteryId;
    in >> m_state;
    switch (m_state) {
    case 0x00:
    case 0x01:
    case 0x02:
    case 0x07:
        in >> m_startDate // 3
                >> m_startMonth // 4
                >> m_startYear // 5
                >> m_startHours // 6
                >> m_startMinutes // 7
                >> m_startSeconds; // 8
        in.skipRawData(9); // 9-17
        in >> m_curCharge; // 18
        in.skipRawData(2); // 19-20
        in >> m_curVoltage; // 21-22
        in.skipRawData(4); // 23-36
        in >> m_startDischargeDegree; //27-28
        in.skipRawData(4); // 29-32
        in >> m_curCap // 33-34
                >> m_effCap // 35-36
                >> m_maxTempCellId // 37
                >> m_maxTemp // 38
                >> m_current // 39-40
                >> m_minVoltageCellId // 41
                >> m_minVoltage // 42
                >> m_maxVoltageCellId //43
                >> m_maxVoltage; // 44
        for (int i = 0; i < 16; i++) {
            in >> m_cellVoltage[i];
        } // 45-60
        in  >> m_loggerTemp1 // 61
                >> m_loggerTemp2 // 62
                >> m_error; // 63
        break;
    case 0x03:
    case 0x04:
    case 0x05:
    case 0x06:
        in >> m_startDate // 3
                >> m_startMonth // 4
                >> m_startYear // 5
                >> m_startHours // 6
                >> m_startMinutes // 7
                >> m_startSeconds; // 8
        in >> m_stopDate // 9
                >> m_stopMonth // 10
                >> m_stopYear // 11
                >> m_stopHours // 12
                >> m_stopMinutes // 13
                >> m_stopSeconds; // 14
        in >> m_durationHours // 15
                >> m_durationMinutes // 16
                >> m_durationSeconds; // 17
        in >> m_startCharge // 18
                >> m_endCharge // 19
                >> m_diffCharge; // 20
        in >> m_startVoltage // 21-22
                >> m_endVoltage // 23-24
                >> m_diffVoltage; //25-26
        in >> m_startDischargeDegree // 27-28
                >> m_endDischargeDegree // 29-30
                >> m_diffDischargeDegree // 31-32
                >> m_minCap // 33-34
                >> m_minEffCap; // 35-36
        in >> m_maxTempCellId // 37
                >> m_maxTemp; // 38
        in >> m_maxCurrent; // 39-40
        in >> m_minVoltageCellId // 41
                >> m_minVoltage; // 42
        in >> m_maxVoltageCellId // 43
                >> m_maxVoltage; // 44
        for (int i = 0; i < 16; i++) {
            in >> m_cellVoltage[i];
        } // 45-60
        in >> m_loggerTemp1 // 61
                >> m_loggerTemp2 // 62
                >> m_error; // 63
        break;
    default:
        break;
    }

}

CurDatum::CurDatum(bsoncxx::document::view b)
{
//	qDebug() << QString::fromStdString(bsoncxx::to_json(b));
	m_batteryId = b["batteryId"].get_int32().value;
	m_state = b["state"]["code"].get_int32().value;
	QDateTime startDate(QDateTime::fromMSecsSinceEpoch(b["dates"]["start"].get_date().to_int64()));
	m_startYear = startDate.date().year() - 2000;
	m_startMonth = startDate.date().month();
	m_startDate = startDate.date().day();
    m_startHours = startDate.time().hour();
    m_startMinutes = startDate.time().minute();
    m_startSeconds = startDate.time().second();

	if (isOfType2()) {
		QDateTime stopDate(QDateTime::fromMSecsSinceEpoch(b["dates"]["stop"].get_date().to_int64()));
		m_stopYear = stopDate.date().year() - 2000;
		m_stopMonth = stopDate.date().month();
		m_stopDate = stopDate.date().day();
    m_stopHours = stopDate.time().hour();
    m_stopMinutes = stopDate.time().minute();
    m_stopSeconds = stopDate.time().second();

		int duration(b["dates"]["duration"].get_int32().value);
        m_durationHours = duration / (3600);
        m_durationMinutes = duration / (60) - m_durationHours * 60;
        m_durationSeconds = duration  - m_durationHours * 3600 - m_durationMinutes * 60;
    }

	if (isOfType2()) {
		m_startCharge = b["charge"]["start"].get_int32().value;
		m_endCharge = b["charge"]["stop"].get_int32().value;
		m_diffCharge = b["charge"]["diff"].get_int32().value;
	} else {
		m_startCharge = b["charge"]["cur"].get_int32().value;
	}

	if (isOfType2()) {
		m_startVoltage = b["voltage"]["start"].get_int32().value;
		m_endVoltage = b["voltage"]["stop"].get_int32().value;
		m_diffVoltage = b["voltage"]["diff"].get_int32().value;
	} else {
		m_startVoltage = b["voltage"]["cur"].get_int32().value;
	}

	if (isOfType2()) {
		m_startDischargeDegree = b["dischargeDegree"]["start"].get_int32().value;
		m_endDischargeDegree = b["dischargeDegree"]["stop"].get_int32().value;
		m_diffDischargeDegree = b["dischargeDegree"]["diff"].get_int32().value;
	} else {
		m_startDischargeDegree = b["dischargeDegree"]["cur"].get_int32().value;
	}

	if (isOfType2()) {
		m_minCap = b["capacity"]["min"].get_int32().value;
		m_minEffCap = b["capacity"]["minEff"].get_int32().value;
	} else {
        m_curCap = b["capacity"]["cur"].get_int32().value;
        m_effCap = b["capacity"]["curEff"].get_int32().value;
	}

	m_current = b["current"].get_int32().value;

	m_minVoltageCellId = b["minVoltage"]["cellId"].get_int32().value;
	m_minVoltage = b["minVoltage"]["value"].get_int32().value;
	m_maxVoltageCellId = b["minVoltage"]["cellId"].get_int32().value;
	m_maxVoltage = b["minVoltage"]["value"].get_int32().value;


	bsoncxx::array::view arr = b["cellVoltage"].get_array().value;
	int i = 0;
	for (bsoncxx::array::element el : arr) {
		m_cellVoltage[i++] = el.get_int32().value;
		if (i == 16) break;
	}

	m_loggerTemp1 = b["loggerTemp"]["min"].get_int32().value;
	m_loggerTemp2 = b["loggerTemp"]["max"].get_int32().value;

	m_error = b["error"]["code"].get_int32().value;


}

QString CurDatum::stateString()
{
    switch (m_state) {
    case 0x00:
        return "Батарея отключена";
    case 0x01:
        return "Батарея подключена";
    case 0x02:
        return "Нет связи";
    case 0x07:
        return "BMS ошибка";
    case 0x03:
        return "Заряжается";
    case 0x04:
        return "Разряжается";
    case 0x05:
        return "Рекуперация";
    case 0x06:
        return "Ждущий режим";

    }

}

QDateTime CurDatum::startDatetime()
{
        QDate date(m_startYear + 2000,
                   m_startMonth,
                   m_startDate);
        QTime time(m_startHours,
                   m_startMinutes,
                   m_startSeconds);
        QDateTime dt(date, time);
        return dt;

}


QDateTime CurDatum::stopDatetime()
{
        QDate date(m_stopYear + 2000,
                   m_stopMonth,
                   m_stopDate);
        QTime time(m_stopHours,
                   m_stopMinutes,
                   m_stopSeconds);
        QDateTime dt(date, time);
        return dt;

}

Duration CurDatum::duration()
{
    return Duration(m_durationHours, m_durationMinutes, m_durationSeconds);
}

bool CurDatum::isOfType2()
{
    switch (m_state) {
    case 0x03:
    case 0x04:
    case 0x05:
    case 0x06:
        return true;
    default:
        return false;
    }

}

QString CurDatum::errorString()
{
    QString s;
    if (!m_error) {
        s = "Ошибок нет";
    } else {
        if (m_error & 1){
            s += "Обрыв при включении в сеть;";
        }
        if (m_error & 2){
            s += "Сработала блокировка;";
        }
        if (m_error & 4){
            s += "Ошибка связи с ячейкой;";
        }
        if (m_error & 8){
            s += "Превышение тока заряда;";
        }
        if (m_error & 16){
            s += "Превышение тока разряда;";
        }
        if (m_error & 32){
            s += "Превышение температуры;";
        }
        if (m_error & 64){
            s += "Низкое напряжение;";
        }
        if (m_error & 128){
            s += "Высокое напряжение;";
        }
    }
    qDebug() << m_error << s;

    return s;
}

QColor CurDatum::color()
{
    switch (m_state) {
        case 0x00:
        return QColor(Qt::darkBlue);
    case 0x01:
        return QColor(Qt::darkGreen);
    case 0x02:
        return QColor(Qt::gray);
    case 0x07:
        return QColor(Qt::darkRed);
    case 0x03:
        return QColor(Qt::blue);
    case 0x04:
        return QColor(Qt::red);
    case 0x05:
        return QColor(Qt::cyan);
    case 0x06:
        return QColor(Qt::magenta);
    }
}

bsoncxx::builder::stream::document CurDatum::toBsonDoc()
{

    bsoncxx::builder::stream::document doc;
    doc << "batteryId" << m_batteryId;

    doc << "state" << open_document
        << "string" << stateString().toStdString()
        << "code" << m_state
        <<
           close_document;

    auto docDates = doc << "dates" << open_document;
    docDates << "start" << BTIME(startDatetime().toMSecsSinceEpoch());
    if (isOfType2()) {
        docDates << "stop" << BTIME(stopDatetime().toMSecsSinceEpoch());
        docDates << "duration" << duration().secs();
    }
    docDates << close_document;

    auto docCharge = doc << "charge" << open_document;
    if (isOfType2()) {
        docCharge << "start" << m_startCharge;
        docCharge << "stop" << m_endCharge;
        docCharge << "diff" << m_diffCharge;
    } else {
        docCharge << "cur" << m_startCharge;
    }
    docCharge << close_document;

    auto docVoltage = doc << "voltage" << open_document;
    if (isOfType2()) {
        docVoltage << "start" << m_startVoltage;
        docVoltage << "stop" << m_endVoltage;
        docVoltage << "diff" << m_diffVoltage;
    } else {
        docVoltage << "cur" << m_startVoltage;
    }
    docVoltage << close_document;

    auto docDischargeDegree = doc << "dischargeDegree" << open_document;
    if (isOfType2()) {
        docDischargeDegree << "start" << m_startDischargeDegree;
        docDischargeDegree << "stop" << m_endDischargeDegree;
        docDischargeDegree << "diff" << m_diffDischargeDegree;
    } else {
        docDischargeDegree << "cur" << m_startDischargeDegree;
    }
    docDischargeDegree << close_document;

    auto docCapacity = doc << "capacity" << open_document;
    if (isOfType2()) {
        docCapacity << "min" << m_minCap;
        docCapacity << "minEff" << m_minEffCap;
    } else {
        docCapacity << "cur" << m_curCap;
        docCapacity << "curEff" << m_effCap;
    }
    docCapacity << close_document;


    doc << "current" << m_current;





    doc << "minVoltage" << open_document
        << "cellId" << m_minVoltageCellId
        << "value" << m_minVoltage
        << close_document;

    doc << "maxVoltage" << open_document
        << "cellId" << m_maxVoltageCellId
        << "value" << m_maxVoltage
        << close_document;

    auto docCellVoltage = doc << "cellVoltage" << open_array;
    for (int i = 0 ; i < 16; i++) {
        docCellVoltage << (quint16)(m_cellVoltage[i]);
    }
    docCellVoltage << close_array;

    doc << "loggerTemp" << open_document
          << "min" << m_loggerTemp1
          << "max" << m_loggerTemp2
          << close_document;

    doc << "error" << open_document
        << "code" << m_error << close_document;


    return doc;
}

qint8 CurDatum::startCharge() const
{
    return m_startCharge;
}

qint16 CurDatum::startVoltage() const
{
    return m_startVoltage;
}

qint16 CurDatum::startDischargeDegree() const
{
    return m_startDischargeDegree;
}

Enums::State CurDatum::state() const
{
    return (Enums::State)(m_state);
}

QSet<Enums::Error> CurDatum::errors() const
{
    QSet<Enums::Error> s;
    if (!m_error) {
    } else {
        if (m_error & 1){
            s << Enums::BreakOnTurningOn;
        }
        if (m_error & 2){
            s << Enums::LockTriggered;
        }
        if (m_error & 4){
            s << Enums::CellConnectError;
        }
        if (m_error & 8){
            s << Enums::ChargeCurrentOver;
        }
        if (m_error & 16){
            s << Enums::DischargeCurrentOver;
        }
        if (m_error & 32){
            s << Enums::TempOver;
        }
        if (m_error & 64){
            s << Enums::LowVoltage;
        }
        if (m_error & 128){
            s << Enums::HighVoltage;
        }
    }

    return s;

}

qint8 CurDatum::endCharge() const
{
    return m_endCharge;
}




}
