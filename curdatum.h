#ifndef CURDATUM_H
#define CURDATUM_H
#include "enums.h"
#include <QDataStream>
#include "duration.h"
#include <QDateTime>
#include <QDate>
#include <QTime>
#include <QColor>
#include <bsoncxx/builder/stream/document.hpp>
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;


namespace logviewer{
class CurDataTable;
class CurDatum
{ public:
    CurDatum(QDataStream& in);
	CurDatum(bsoncxx::document::view b);
    friend class CurDataTable;
    friend class Database;
    QString stateString();
    QDateTime startDatetime();
    QDateTime stopDatetime();
    Duration duration();
    bool isOfType2();
    QString errorString();
    QColor color();
    bsoncxx::builder::stream::document toBsonDoc();
    qint8 startCharge() const;

    qint16 startVoltage() const;

    qint16 startDischargeDegree() const;

    Enums::State state() const;
    QSet<Enums::Error> errors() const;

    qint8 endCharge() const;

private:
    quint16 m_batteryId;
    quint8 m_state;
    quint8 m_startDate;
    quint8 m_startMonth;
    quint8 m_startYear;
    quint8 m_startHours;
    quint8 m_startMinutes;
    quint8 m_startSeconds;
    quint8 m_stopDate;
    quint8 m_stopMonth;
    quint8 m_stopYear;
    quint8 m_stopHours;
	quint8 m_stopMinutes;
    quint8 m_stopSeconds;
    quint8 m_durationHours;
    quint8 m_durationMinutes;
    quint8 m_durationSeconds;
    qint8 m_startCharge;
    qint8 m_curCharge;
    qint8 m_endCharge;
    qint8 m_diffCharge;
    qint16 m_startVoltage;
    qint16 m_curVoltage;
    qint16 m_endVoltage;
    qint16 m_diffVoltage;
    qint16 m_startDischargeDegree;
    qint16 m_endDischargeDegree;
    qint16 m_diffDischargeDegree;
    qint16 m_minCap;
    qint16 m_minEffCap;
    qint16 m_curCap;
    quint16 m_effCap;
    qint8 m_maxTempCellId;
    qint8 m_maxTemp;
    qint16 m_current;
    qint16 m_maxCurrent;
    qint8 m_minVoltageCellId;
    quint8 m_minVoltage;
    qint8 m_maxVoltageCellId;
    quint8 m_maxVoltage;
    qint8 m_loggerTemp1;
    qint8 m_loggerTemp2;
    qint8 m_error;
    quint8 m_cellVoltage[16];
};


}
#endif // CURDATUM_H
