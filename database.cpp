#include "database.h"
#include <iostream>
#include <chrono>
#include <QFile>
#include <QDataStream>
#include <QTextStream>
#include <QtDebug>
#include <QChartView>
#include <QDateTime>
#include <QSplineSeries>
#include <QDate>
#include <QTime>
#include "curdatum.h"
#include <QDateTimeAxis>
#include <QValueAxis>
#include <QXYSeries>
#include <QScatterSeries>
#include <QHeaderView>
#include "lvapp.h"

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/types.hpp>
#include <mongocxx/collection.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
using  bsoncxx::builder::stream::document;
using  bsoncxx::builder::stream::open_document;
using  bsoncxx::builder::stream::close_document;
using  bsoncxx::builder::stream::open_array;
using  bsoncxx::builder::stream::close_array;
using  bsoncxx::builder::stream::finalize;
using namespace QtCharts;

namespace logviewer {

Database::Database()
{


    initConnection();
    queryBatteries();


}

mongocxx::database Database::mongoDB()
{
    return m_db;

}

void Database::initConnection()
{
//    inst = mongocxx::instance{};
//    mongocxx::instance inst{};
    client = mongocxx::client{mongocxx::uri{}};
//    mongocxx::client conn{mongocxx::uri{}};

    m_db = client["logviewer"];

}

void Database::queryBatteries()
{
    mongocxx::options::find opts{};
    auto filter = document{};
    opts.projection(document{} << "client" << 1 << "nomVoltage" << 1 << "nomCapacity" << 1 << finalize);
    auto cursor = m_db["batteries"].find({});

    for (auto&& doc : cursor) {
//        std::cout << bsoncxx::to_json(doc) << std::endl;
        Battery* b = new Battery(doc, m_db);
        m_batteries.insert(b->batteryId(), b);

    }

}

void Database::exportCurDataToMongo()
{
//    mongocxx::instance inst{};
//    mongocxx::client conn{mongocxx::uri{}};
//    auto db = conn["logviewer"];
    foreach (CurDatum* d, m_curData) {
        bsoncxx::builder::stream::document doc = d->toBsonDoc();

		auto res = m_db["curData"].insert_one(doc.view());

    }

}

void Database::exportLifeDataToMongo()
{

	foreach (LifeDatum* d, m_lifeData) {
		qDebug() << "one";
		bsoncxx::builder::stream::document doc = d->toBsonDoc();

		auto res = m_db["lifeData"].insert_one(doc.view());
	}

}

void Database::importCurData(QString filename)
{
    QFile f(filename);
    if (!f.open(QFile::ReadOnly)) {
        qWarning() << "Couldn't open file " << filename << "\nReason: " << f.errorString();
    }

    QDataStream in(&f);
    while (!in.atEnd()) {
        CurDatum* curDatum = new CurDatum(in);
        m_curData << curDatum;

    }
    qDebug() << m_curData.size();


}

void Database::importLifeData(QString filename)
{
    QFile f(filename);
    if (!f.open(QFile::ReadOnly)) {
        qWarning() << "Couldn't open file " << filename << "\nReason: " << f.errorString();
    }

    QDataStream in(&f);
    while (!in.atEnd()) {
        LifeDatum* lifeDatum = new LifeDatum(in);
		m_lifeData << lifeDatum;
    }


}


QVector<CurDatum *> *Database::curData()
{
    return &m_curData;
}

QMap<int, Battery *> Database::batteries()
{

	return m_batteries;
}

QVector<CurDatum*> Database::queryCurData(int batteryId)
{
	QVector<CurDatum*> data;
	bsoncxx::builder::stream::document doc;
	doc << "batteryId" << batteryId;

	auto cursor = m_db["curData"].find(doc.view());


	for (bsoncxx::document::view doc : cursor) {

		CurDatum* d = new CurDatum(doc);
		data << d;
	}

	return data;

}

void Database::dropDB()
{
	m_db["curData"].drop();
	m_db["lifeData"].drop();
}


}
