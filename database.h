#ifndef DATABASE_H
#define DATABASE_H
#include <QVector>
#include "curdatum.h"
#include "lifedatum.h"
#include <QTableWidget>
#include <battery.h>
#include <bsoncxx/builder/stream/document.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/pipeline.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/document/element.hpp>
#include <bsoncxx/types/value.hpp>
#include <QObject>
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;


namespace logviewer {
class Database: public QObject
{
	Q_OBJECT
public:
    Database();
    mongocxx::database mongoDB();
    void exportCurDataToMongo();
	void exportLifeDataToMongo();
    void importCurData(QString filename);
    void importLifeData(QString filename);
    QVector<CurDatum *>* curData();
    QMap<int, Battery*> batteries();
	QVector<CurDatum *> queryCurData(int batteryId);

public slots:
	void dropDB();

private:
    void initConnection();
    void queryBatteries();
private:
    QMap<int, Battery *> m_batteries;
    QVector<CurDatum*> m_curData;
    QVector<LifeDatum*> m_lifeData;
    mongocxx::database m_db;
    mongocxx::instance inst;
    mongocxx::client client;


};
}

#endif // DATABASE_H
