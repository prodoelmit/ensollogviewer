#include "dischargestatstable.h"
#include <QHeaderView>
#include "lvapp.h"
#include "qdebug.h"
#include <QScrollBar>

namespace logviewer {
DischargeStatsTable::DischargeStatsTable(int batteryId)
    : QTableWidget()
    , m_batteryId(batteryId)
{
//    horizontalHeader()->setStretchLastSection(true);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_fromDatetime.setMSecsSinceEpoch(0);
    m_toDatetime = QDateTime::currentDateTime();
    for (int i = 0; i < 10; i++) {
        m_cycleCounts[i] = 0;
    }

    fillTable();



}

void DischargeStatsTable::prepareData()
{
    auto data = db->queryCurData(m_batteryId);

    int curStartCharge = 0;
    int curEndCharge = 0;
    int prevStartCharge = 0;
    int prevEndCharge = 0;
    bool first = true;
    foreach (CurDatum* d, data) {
        if (d->state() != Enums::Charging) continue;
        if (d->duration().secs() < 60) continue;
        if (d->stopDatetime() < m_fromDatetime) continue;
        if (d->startDatetime() > m_toDatetime) continue;

        if (first) {
            first = false;
        curStartCharge = d->startCharge();
        curEndCharge = d->endCharge();
        continue;
        }

        prevStartCharge = curStartCharge;
        prevEndCharge = curEndCharge;
        curStartCharge = d->startCharge();
        curEndCharge = d->endCharge();

        int depth = prevEndCharge - curStartCharge;

        int range = depth / 10;
        if (range >= 10) {
            qDebug() << depth << range ;

            range = 9;
        }
        if (range < 0) { qDebug() << depth << range;
            range = 0 ;
        }

        m_cycleCounts[range] += 1;


    }

}

void DischargeStatsTable::setDatetimes(QDateTime &d1, QDateTime &d2)
{

}

void DischargeStatsTable::refresh()
{
    clear();
    prepareData();
    fillTable();

}

void DischargeStatsTable::fillTable()
{

    setColumnCount(2);
    setRowCount(10);
    for (int i = 0; i < 10; i++) {
        QString rangeText = QString("%1-%2").arg(10*i).arg(10*(i+1));
        int count = m_cycleCounts[i];

        auto i1 = new QTableWidgetItem(rangeText);
        auto i2 = new QTableWidgetItem(QString::number(count));
        i1->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

        i1->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        i2->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);

        setItem(i, 0, i1);
        setItem(i, 1, i2);

    }
    {
        int h = verticalHeader()->length() + horizontalHeader()->height() + contentsMargins().top() + contentsMargins().bottom();
        setFixedHeight(h);
    }

    horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

//    adjustSize();

}

void DischargeStatsTable::resizeEvent(QResizeEvent *event)
{
    int  sum = 0;
    for (int i = 0; i < rowCount(); i++) {
        sum += verticalHeader()->sectionSize(i);
    }
    qDebug() << sum;
    qDebug() <<  verticalScrollBar()->maximum() - verticalScrollBar()->minimum();
    QTableWidget::resizeEvent(event);
}

}
