#ifndef DISCHARGESTATSTABLE_H
#define DISCHARGESTATSTABLE_H
#include <QTableWidget>
#include <QDateTime>


namespace logviewer {
class DischargeStatsTable: public QTableWidget
{
    Q_OBJECT
public:
    DischargeStatsTable(int batteryId);


    void prepareData();
    void setDatetimes(QDateTime& d1, QDateTime& d2);

    void refresh();
    void fillTable();
private:
    int m_batteryId;
    QDateTime m_fromDatetime;
    QDateTime m_toDatetime;

    int m_cycleCounts[10];

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);
};
}

#endif // DISCHARGESTATSTABLE_H
