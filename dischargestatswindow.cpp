#include "dischargestatswindow.h"
#include <QLabel>

namespace logviewer{
DischargeStatsWindow::DischargeStatsWindow(int batteryId)
    : QWidget()
{
    setWindowTitle("Статистика циклов по глубине разряда");
    m_layout = new QGridLayout();
    setLayout(m_layout);

    m_table = new DischargeStatsTable(batteryId);

    m_fromDateEdit = new QDateTimeEdit(QDateTime::fromMSecsSinceEpoch(0));
    m_fromDateEdit->setDisplayFormat("dd.MM.yyyy hh:mm");
    m_fromDateEdit->setCalendarPopup(true);
    m_toDateEdit = new QDateTimeEdit(QDateTime::currentDateTime());
    m_toDateEdit->setDisplayFormat("dd.MM.yyyy hh:mm");
    m_toDateEdit->setCalendarPopup(true);

    connect(m_fromDateEdit, &QDateTimeEdit::editingFinished,
            this, &DischargeStatsWindow::updateTable);
    connect(m_toDateEdit, &QDateTimeEdit::editingFinished,
            this, &DischargeStatsWindow::updateTable);

    QLabel* l1 = new QLabel("Начало интервала");
    QLabel* l2 = new QLabel("Конец интервала");

//    m_layout->setRowStretch(0, 1);
    m_layout->setRowStretch(2, 1);
    m_layout->setRowStretch(1, 0);
    m_layout->addWidget(m_table, 0, 0, 1, 2);
    m_layout->addWidget(l1, 1, 0);
    m_layout->addWidget(l2, 1, 1);
    m_layout->addWidget(m_fromDateEdit, 2, 0);
    m_layout->addWidget(m_toDateEdit, 2, 1);
    m_table->refresh();
    show();
    setFixedHeight(height());
    setFixedWidth(width());


}

void DischargeStatsWindow::updateTable()
{
    QDateTime fromDateTime = m_fromDateEdit->dateTime();
    QDateTime toDateTime = m_toDateEdit->dateTime();

    m_table->setDatetimes(fromDateTime, toDateTime);
    m_table->refresh();




}

}
