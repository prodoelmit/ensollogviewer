#ifndef DISCHARGESTATSWINDOW_H
#define DISCHARGESTATSWINDOW_H
#include <QGridLayout>
#include "dischargestatstable.h"
#include <QDateTimeEdit>


namespace logviewer{
class DischargeStatsWindow: public QWidget
{
    Q_OBJECT
public:
    DischargeStatsWindow(int batteryId);
public slots:
    void updateTable();
private:
    QGridLayout* m_layout;
    DischargeStatsTable* m_table;

    QDateTimeEdit* m_fromDateEdit;
    QDateTimeEdit* m_toDateEdit;
};

}
#endif // DISCHARGESTATSWINDOW_H
