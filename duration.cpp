#include "duration.h"
#include <QBoxLayout>
#include <QLabel>
#include <QHBoxLayout>

namespace logviewer {
Duration::Duration()
    : m_secs(0)
{
    QMetaType::registerConverter(&Duration::toString);
    qRegisterMetaType<logviewer::Duration>("Duration");

}

Duration::Duration(int secs)
    : m_secs(secs)
{
    QMetaType::registerConverter(&Duration::toString);


}

Duration::Duration(int h, int m, int s)
    : Duration(3600* h + 60 * m + s)
{

}


Duration::Duration(const Duration &other)
    : Duration(other.m_secs)
{

}

bool Duration::operator>(Duration &other)
{
    return (m_secs > other.secs());
}

bool Duration::operator<(Duration &other)
{
    return (m_secs < other.secs());

}

bool Duration::operator==(Duration &other)
{
    return (m_secs == other.secs());
}

QString Duration::toString() const
{
    QString s = QString("%2:%3:%4").arg(hours()).arg(minutes()).arg(seconds());
    return s;
}


Duration::~Duration()
{

}

// int Duration::days() const
//{
//     return m_secs / (3600 * 24);
// }

 int Duration::hours() const
 {
     return m_secs / (3600);

 }

 int Duration::minutes() const
 {
     return m_secs / (60) - hours() * 60;

 }


 int Duration::seconds() const
 {
     return m_secs - hours() * (3600) - minutes() * (60);

 }

int Duration::secs() const
{
    return m_secs;

}

DurationEdit::DurationEdit()
    : QWidget()
{
    QHBoxLayout* m_layout = new QHBoxLayout;
    setLayout(m_layout);

    m_hoursEdit = new QSpinBox();
    m_hoursEdit->setMinimum(0);
    m_hoursEdit->setMaximum(10 * 365 * 24);
    m_hoursEdit->setValue(0);
    m_hoursEdit->setAlignment(Qt::AlignRight);

    m_minutesEdit = new QSpinBox();
    m_minutesEdit->setRange(0, 59);
    m_minutesEdit->setValue(0);

    m_secondsEdit = new QSpinBox();
    m_secondsEdit->setRange(0, 59);
    m_secondsEdit->setValue(0);

    QLabel* betweenHM = new QLabel(":");
    QLabel* betweenMS = new QLabel(":");

    m_layout->addWidget(m_hoursEdit);
    m_layout->addWidget(betweenHM);
    m_layout->addWidget(m_minutesEdit);
    m_layout->addWidget(betweenMS);
    m_layout->addWidget(m_secondsEdit);
}

Duration DurationEdit::duration()
{
    int s = m_secondsEdit->value();
    int m = m_minutesEdit->value();
    int h = m_hoursEdit->value();

    return Duration(h,m,s);

}

}
