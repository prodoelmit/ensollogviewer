#ifndef DURATION_H
#define DURATION_H
#include <QSpinBox>
#include <QMetaType>
#include <QString>
#include <QDateTimeEdit>


namespace logviewer {
class Duration
{
public:
    Duration();
    Duration(int secs);
    Duration(int h, int m, int s);
    Duration(const Duration &other);
    bool operator>(Duration& other);
    bool operator<(Duration& other);
    bool operator==(Duration& other);
    QString toString() const;
    ~Duration();
//    int days() const;
    int hours() const;
    int minutes() const;
    int seconds() const;
    int secs() const;
private:

    int m_secs;

};
class DurationEdit:public QWidget {
public:

    DurationEdit();
    DurationEdit(Duration dur);
    Duration duration();
private:
    QSpinBox* m_hoursEdit;
    QSpinBox* m_minutesEdit;
    QSpinBox* m_secondsEdit;


};
}

using namespace logviewer;
Q_DECLARE_METATYPE(Duration)


#endif // DURATION_H
