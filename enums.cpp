#include "enums.h"
#include "qdebug.h"

namespace logviewer {

QList<Enums::CurDatumField> Enums::allFieldsList()
{
    QList<Enums::CurDatumField> fields;
    for (int i = 1; i <= (int)(Enums::ErrorCode); i++ ) {
        fields << (Enums::CurDatumField)(i);
    }
    return fields;
}

Enums::FieldType Enums::type(Enums::CurDatumField f)
{
    switch (f) {
    case Enums::BatteryId: return NumberType;
    case Enums::StateCode: return EnumType;
    case Enums::StartDatetime: return DateType;
    case Enums::StopDatetime: return DateType;
    case Enums::Duration: return DurationType;
    case Enums::StartCharge: return NumberType;
    case Enums::EndCharge: return NumberType;
    case Enums::DiffCharge: return NumberType;
    case Enums::StartVoltage: return NumberType;
    case Enums::EndVoltage: return NumberType;
    case Enums::DiffVoltage: return NumberType;
    case Enums::StartDischargeDegree: return NumberType;
    case Enums::EndDischargeDegree: return NumberType;
    case Enums::DiffDischargeDegree: return NumberType;
    case Enums::MinCapacity: return NumberType;
    case Enums::MinEffCapacity: return NumberType;
    case Enums::MaxTempCellId: return NumberType;
    case Enums::MaxTemp: return NumberType;
    case Enums::MaxCurrent: return NumberType;
    case Enums::MinVoltageCellId: return NumberType;
    case Enums::MinVoltage: return NumberType;
    case Enums::MaxVoltageCellId: return NumberType;
    case Enums::MaxVoltage: return NumberType;
    case Enums::CellVoltage1: return NumberType;
    case Enums::CellVoltage2: return NumberType;
    case Enums::CellVoltage3: return NumberType;
    case Enums::CellVoltage4: return NumberType;
    case Enums::CellVoltage5: return NumberType;
    case Enums::CellVoltage6: return NumberType;
    case Enums::CellVoltage7: return NumberType;
    case Enums::CellVoltage8: return NumberType;
    case Enums::CellVoltage9: return NumberType;
    case Enums::CellVoltage10: return NumberType;
    case Enums::CellVoltage11: return NumberType;
    case Enums::CellVoltage12: return NumberType;
    case Enums::CellVoltage13: return NumberType;
    case Enums::CellVoltage14: return NumberType;
    case Enums::CellVoltage15: return NumberType;
    case Enums::CellVoltage16: return NumberType;
    case Enums::MinLoggerTemp: return NumberType;
    case Enums::MaxLoggerTemp: return NumberType;
    case Enums::ErrorCode: return EnumType;
    };

}

QMap<int, QString> Enums::stringValuesForEnum(Enums::CurDatumField f)
{
    QMap<int, QString> values;
    switch (f) {
    case StateCode:
        for (int i = 0; i <= 0x07; i++) {
            values.insert(i, stateString(i));
        }
        break;
    case ErrorCode:
        for (int i = 0; i < 8; i++) {
            values.insert(1 << i, errorString(1 << i));
        }
    }

    return values;

}

QString Enums::toString(Enums::CurDatumField f)
{
    switch (f) {
    case Enums::BatteryId: return "ID батареи";
    case Enums::StateCode: return  "Состояние";
    case Enums::StartDatetime: return "Начало";
    case Enums::StopDatetime: return "Конец";
    case Enums::Duration: return "Длительность";
    case Enums::StartCharge: return "Начальный заряд";
    case Enums::EndCharge: return "Конечный заряд";
    case Enums::DiffCharge: return "Разность заряда";
    case Enums::StartVoltage: return "Начальное напряжение";
    case Enums::EndVoltage: return "Конечное напряжение";
    case Enums::DiffVoltage: return "Разность напряжений";
    case Enums::StartDischargeDegree: return "Начальная степень разряда";
    case Enums::EndDischargeDegree: return "Конечная степень разряда";
    case Enums::DiffDischargeDegree: return "Разность степени разряда";
    case Enums::MinCapacity: return "Емкость";
    case Enums::MinEffCapacity: return "Эффективная емкость";
    case Enums::MaxTempCellId: return "Ячейка с макс.температурой";
    case Enums::MaxTemp: return "Макс. температура";
    case Enums::MaxCurrent: return "Ток";
    case Enums::MinVoltageCellId: return "Ячейка с минимальным напряжением";
    case Enums::MinVoltage: return "Минимальное напряжение";
    case Enums::MaxVoltageCellId: return "Ячейка с максимальным напряжением";
    case Enums::MaxVoltage: return "Максимальное напряжение";
    case Enums::CellVoltage1: return "Напряжение в ячейке 1";
    case Enums::CellVoltage2: return "Напряжение в ячейке 2";
    case Enums::CellVoltage3: return "Напряжение в ячейке 3";
    case Enums::CellVoltage4: return "Напряжение в ячейке 4";
    case Enums::CellVoltage5: return "Напряжение в ячейке 5";
    case Enums::CellVoltage6: return "Напряжение в ячейке 6";
    case Enums::CellVoltage7: return "Напряжение в ячейке 7";
    case Enums::CellVoltage8: return "Напряжение в ячейке 8";
    case Enums::CellVoltage9: return "Напряжение в ячейке 9";
    case Enums::CellVoltage10: return "Напряжение в ячейке 10";
    case Enums::CellVoltage11: return "Напряжение в ячейке 11";
    case Enums::CellVoltage12: return "Напряжение в ячейке 12";
    case Enums::CellVoltage13: return "Напряжение в ячейке 13";
    case Enums::CellVoltage14: return "Напряжение в ячейке 14";
    case Enums::CellVoltage15: return "Напряжение в ячейке 15";
    case Enums::CellVoltage16: return "Напряжение в ячейке 16";
    case Enums::MinLoggerTemp: return "Мин.температура логгера";
    case Enums::MaxLoggerTemp: return "Макс.температура логгера";
    case Enums::ErrorCode: return "Ошибки";
    }

}

QString Enums::stateString(int state)
{
    switch (state) {
    case 0x00:
        return "Батарея отключена";
    case 0x01:
        return "Батарея подключена";
    case 0x02:
        return "Нет связи";
    case 0x07:
        return "BMS ошибка";
    case 0x03:
        return "Заряжается";
    case 0x04:
        return "Разряжается";
    case 0x05:
        return "Рекуперация";
    case 0x06:
        return "Ждущий режим";


    }

}

QString Enums::errorString(int code)
{
    if (!code) {
        return "Ошибок нет";
    } else
    if (code & 1){
        return "Обрыв при включении в сеть;";
    } else
    if (code & 2){
        return "Сработала блокировка;";
    } else
    if (code & 4){
        return "Ошибка связи с ячейкой;";
    } else
    if (code & 8){
        return "Превышение тока заряда;";
    } else
    if (code & 16){
        return "Превышение тока разряда;";
    } else
    if (code & 32){
        return "Превышение температуры;";
    } else
    if (code & 64){
        return "Низкое напряжение;";
    } else
    if (code & 128){
        return "Высокое напряжение;";
    }



}


}
