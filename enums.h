#ifndef ENUMS_H
#define ENUMS_H
#include <QStringList>


namespace logviewer {
class Enums
{
public:
    enum CurDatumField
    { BatteryId = 1
        , StateCode
        , StartDatetime
        , StopDatetime
        , Duration
        , StartCharge
        , EndCharge
        , DiffCharge
        , StartVoltage
        , EndVoltage
        , DiffVoltage
        , StartDischargeDegree , EndDischargeDegree
        , DiffDischargeDegree
        , MinCapacity
        , MinEffCapacity
        , MaxTempCellId
        , MaxTemp
        , MaxCurrent
        , MinVoltageCellId
        , MinVoltage
        , MaxVoltageCellId
        , MaxVoltage
        , CellVoltage1
        , CellVoltage2
        , CellVoltage3
        , CellVoltage4
        , CellVoltage5
        , CellVoltage6
        , CellVoltage7
        , CellVoltage8
        , CellVoltage9
        , CellVoltage10
        , CellVoltage11
        , CellVoltage12
        , CellVoltage13
        , CellVoltage14
        , CellVoltage15
        , CellVoltage16
        , MinLoggerTemp
        , MaxLoggerTemp
        , ErrorCode
    };

    enum FieldType {
        String = 1
        , DateType
        , NumberType
        , DurationType
        , EnumType
    };
    enum NumberRelationships {
        EQ = 1
        , Greater
        , Less
        , NEQ
        , NumberMinMax
    };
    enum DateRelationships {
        Later
        , Earlier
        , Between
        , DateMinMax
    };

    enum TimeRelationships {
        Longer,
        Shorter,
        TimeBetween,
        TimeMinMax
    };

    enum EnumRelationships {
        EnumAmong,
        EnumNotAmong,
        EnumEquals,
    };

    enum State {
        Disconnected = 0x0
        , Connected = 0x01
        , NoBMSLink = 0x02
        , BMSError = 0x07
        , Charging = 0x03
        , Discharging = 0x04
        , Recuperating = 0x05
        , Standby = 0x06
    };

    enum Error {
        NoError = 0x00
        , BreakOnTurningOn = 1
        , LockTriggered = 2
        , CellConnectError = 4
        , ChargeCurrentOver = 8
        , DischargeCurrentOver = 16
        , TempOver = 32
        , LowVoltage = 64
        , HighVoltage = 128
    };


    static QList<CurDatumField> allFieldsList();
    static FieldType type(CurDatumField f);
    static QMap<int, QString> stringValuesForEnum(CurDatumField f);
    static QString toString(CurDatumField f);
    static CurDatumField fromString(QString s);
    static QString stateString(int state);
    static QString errorString(int code);
};

}

#endif // ENUMS_H
