#include "filterwidget.h"
#include <QMap>
#include <QHBoxLayout>
#include "enums.h"
#include <QTime>


namespace logviewer {

FilterWidget::FilterWidget(Enums::CurDatumField f)
    : QWidget()
    , m_field(f)
{
    m_layout = new QHBoxLayout();
    setLayout(m_layout);

    initLabel();
    initStatements();

    m_destroyButton = new QPushButton("x");
    connect(m_destroyButton, &QPushButton::clicked,
            [this](){
        deleteLater();
    });

    m_layout->addWidget(m_destroyButton, 0);
    m_layout->addWidget(m_label , 1);
    m_layout->addWidget(m_stmt, 2);
    initEdits();
    updateFilterInputs();


}

void FilterWidget::updateFilterInputs()
{
    auto t = Enums::type(m_field);
    auto s = m_stmt->currentData().toInt();
    switch (t) {
    case Enums::NumberType:
        switch ((Enums::NumberRelationships)(s)) {
        case Enums::EQ:
            showEdit(m_eqNumber);
            break;
        case Enums::Greater:
            showEdit(m_fromNumber);
            break;
        case Enums::Less:
            showEdit(m_toNumber);
            break;
        case Enums::NEQ:
            showEdit(m_eqNumber);
            break;
        case Enums::NumberMinMax:
            showEdits(QSet<QWidget*>());
            break;
        }
        break;
    case Enums::DateType:
        switch ((Enums::DateRelationships)(s)) {
        case Enums::Later:
            showEdit(m_startDate);
            break;
        case Enums::Earlier:
            showEdit(m_endDate);
            break;
        case Enums::Between:
            showEdits(QSet<QWidget*>() << m_startDate << m_endDate);
            break;
        case Enums::DateMinMax:
            showEdits(QSet<QWidget*>());
            break;
        }
        break;
    case Enums::DurationType:
        switch ((Enums::TimeRelationships)(s)) {
        case Enums::Longer:
            showEdit(m_fromTime);
            break;
        case Enums::Shorter:
            showEdit(m_toTime);
            break;
        case Enums::TimeBetween:
            showEdits(QSet<QWidget*>() << m_fromTime << m_toTime);
            break;
        case Enums::TimeMinMax:
            showEdits(QSet<QWidget*>());
            break;
        }
    }


}


void FilterWidget::initLabel()
{
    QString label;
    switch (m_field) {
    case Enums::StateCode: label = tr( "Состояние"); break;
    case Enums::StartDatetime: label = tr( "Начало"); break;
    case Enums::StopDatetime: label = tr( "Конец"); break;
    case Enums::Duration: label = tr( "Длительность"); break;
    case Enums::StartCharge: label = tr( "Начальный заряд"); break;
    case Enums::EndCharge: label = tr( "Конечный заряд"); break;
    case Enums::DiffCharge: label = tr( "Разность зарядов"); break;
    case Enums::StartVoltage: label = tr( "Начальное напряжение"); break;
    case Enums::EndVoltage: label = tr( "Конечное напряжение"); break;
    case Enums::DiffVoltage: label = tr( "Разность напряжений"); break;
    case Enums::StartDischargeDegree: label = tr( "Начальная степень разряда"); break;
    case Enums::EndDischargeDegree: label = tr( "Конечная степень разряда"); break;
    case Enums::DiffDischargeDegree: label = tr( "Разность степени разряда"); break;
    case Enums::MinCapacity: label = tr( "Емкость"); break;
    case Enums::MinEffCapacity: label = tr( "Эффективная емкость"); break;
    case Enums::MaxTempCellId: label = tr( "Ячейка с макс.температурой"); break;
    case Enums::MaxTemp: label = tr( "Макс. температура"); break;
    case Enums::MaxCurrent: label = tr( "Ток"); break;
    case Enums::MinVoltageCellId: label = tr( "Ячейка с минимальным напряжением"); break;
    case Enums::MinVoltage: label = tr( "Минимальное напряжение"); break;
    case Enums::MaxVoltageCellId: label = tr( "Ячейка с максимальным напряжением"); break;
    case Enums::MaxVoltage: label = tr( "Максимальное напряжение"); break;
    case Enums::CellVoltage1: label = tr( "Напряжение в ячейке 1"); break;
    case Enums::CellVoltage2: label = tr( "Напряжение в ячейке 2"); break;
    case Enums::CellVoltage3: label = tr( "Напряжение в ячейке 3"); break;
    case Enums::CellVoltage4: label = tr( "Напряжение в ячейке 4"); break;
    case Enums::CellVoltage5: label = tr( "Напряжение в ячейке 5"); break;
    case Enums::CellVoltage6: label = tr( "Напряжение в ячейке 6"); break;
    case Enums::CellVoltage7: label = tr( "Напряжение в ячейке 7"); break;
    case Enums::CellVoltage8: label = tr( "Напряжение в ячейке 8"); break;
    case Enums::CellVoltage9: label = tr( "Напряжение в ячейке 9"); break;
    case Enums::CellVoltage10: label = tr( "Напряжение в ячейке 10"); break;
    case Enums::CellVoltage11: label = tr( "Напряжение в ячейке 11"); break;
    case Enums::CellVoltage12: label = tr( "Напряжение в ячейке 12"); break;
    case Enums::CellVoltage13: label = tr( "Напряжение в ячейке 13"); break;
    case Enums::CellVoltage14: label = tr( "Напряжение в ячейке 14"); break;
    case Enums::CellVoltage15: label = tr( "Напряжение в ячейке 15"); break;
    case Enums::CellVoltage16: label = tr( "Напряжение в ячейке 16"); break;
    case Enums::MinLoggerTemp: label = tr( "Мин.температура логгера"); break;
    case Enums::MaxLoggerTemp: label = tr( "Макс.температура логгера"); break;
    case Enums::ErrorCode: label = tr( "Ошибки"); break;

    }

    m_label = new QLabel(label);

}

void FilterWidget::initStatements()
{
    m_stmt = new QComboBox;
    switch (Enums::type(m_field)) {
    case Enums::NumberType:
        m_stmt->addItem(tr("соответствует"), Enums::EQ);
        m_stmt->addItem(tr("не соответствует"), Enums::NEQ);
        m_stmt->addItem(tr("больше, чем"), Enums::Greater);
        m_stmt->addItem(tr("меньше, чем"), Enums::Less);
        m_stmt->addItem(tr("только мин/макс/значения"), Enums::NumberMinMax);
        break;
    case Enums::DateType:
        m_stmt->addItem(tr("раньше, чем"), Enums::Earlier);
        m_stmt->addItem(tr("позже, чем"), Enums::Later);
        m_stmt->addItem(tr("между"), Enums::Between);
        m_stmt->addItem(tr("только мин/макс/значения"), Enums::DateMinMax);
        break;
    case Enums::EnumType:
        m_stmt->addItem(tr("среди значений"), Enums::EnumAmong);
        m_stmt->addItem(tr("не среди значений"), Enums::EnumNotAmong);
        m_stmt->addItem(tr("соответсвует"), Enums::EnumEquals);
        break;
    case Enums::DurationType:
        m_stmt->addItem(tr("дольше, чем"), Enums::Longer);
        m_stmt->addItem(tr("короче, чем"), Enums::Shorter);
        m_stmt->addItem(tr("между"), Enums::TimeBetween);
        m_stmt->addItem(tr("только мин/макс/значения"), Enums::TimeMinMax);
        break;
    }

    connect(m_stmt, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &FilterWidget::updateFilterInputs);


}

void FilterWidget::initEdits()
{
    switch (Enums::type(m_field)) {
    case Enums::DateType:
    m_startDate = new QDateTimeEdit(QDateTime::fromMSecsSinceEpoch(0));
    m_startDate->setCalendarPopup(true);
    m_startDate->setDisplayFormat("dd-MM-yyyy hh:mm");
    m_endDate = new QDateTimeEdit(QDateTime::currentDateTime());
    m_endDate->setDisplayFormat("dd-MM-yyyy hh:mm");
    m_endDate->setCalendarPopup(true);

        m_layout->insertWidget(3, m_startDate);
        m_layout->insertWidget(4, m_endDate);
        m_edits << m_startDate;
        m_edits << m_endDate;
        break;
    case Enums::NumberType:
        m_eqNumber = new QSpinBox();
        m_fromNumber = new QSpinBox();
        m_toNumber = new QSpinBox();
        m_eqNumber->setRange(-INT32_MAX, INT32_MAX);
        m_fromNumber->setRange(-INT32_MAX, INT32_MAX);
        m_toNumber->setRange(-INT32_MAX, INT32_MAX);
        m_layout->insertWidget(3, m_eqNumber);
        m_layout->insertWidget(3, m_fromNumber);
        m_layout->insertWidget(4, m_toNumber);
        m_edits << m_eqNumber << m_fromNumber << m_toNumber;
        break;
    case Enums::DurationType:
        m_fromTime = new DurationEdit();
        m_toTime = new DurationEdit();
        m_layout->insertWidget(3, m_fromTime);
        m_layout->insertWidget(4, m_toTime);
        m_edits << m_fromTime << m_toTime;
        break;
    case Enums::EnumType:
        m_enumList = new QListWidget();
        m_enumList->setSelectionMode(QListWidget::MultiSelection);
        QMap<int, QString> items = Enums::stringValuesForEnum(m_field);
        for (auto it = items.begin(); it != items.end(); ++it) {
            QListWidgetItem* item = new QListWidgetItem();
            item->setText(it.value());
            item->setData(Qt::UserRole, it.key());
            m_enumList->addItem(item);
        }
        m_layout->insertWidget(3, m_enumList);
        m_edits << m_enumList;
        break;
    }

}

void FilterWidget::showEdit(QWidget *w)
{
    foreach (auto e, m_edits) {
        if (w == e) {
            e->show();
        } else {
            e->hide();
        }
    }

}

void FilterWidget::showEdits(QSet<QWidget *> ws)
{
    foreach (auto e, m_edits) {
        if (ws.contains(e)) {
            e->show();
        } else {
            e->hide();
        }
    }
}

Enums::CurDatumField FilterWidget::field()
{

    return m_field;
}

int FilterWidget::statementInt()
{

    return m_stmt->currentData().toInt();
}

QVariant FilterWidget::value1()
{
    auto t = Enums::type(m_field);
    auto s = m_stmt->currentData().toInt();
    switch (t) {
    case Enums::NumberType:
        switch ((Enums::NumberRelationships)(s)) {
        case Enums::EQ:
            return m_eqNumber->value();
            break;
        case Enums::Greater:
            return m_fromNumber->value();
            break;
        case Enums::Less:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::NEQ:
            return m_eqNumber->value();
            break;
        case Enums::NumberMinMax:
            return QVariant(QVariant::Invalid);
            break;
        }
        break;
    case Enums::DateType:
        switch ((Enums::DateRelationships)(s)) {
        case Enums::Later:
            return m_startDate->dateTime();
            break;
        case Enums::Earlier:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::Between:
            return m_startDate->dateTime();
            break;
        case Enums::DateMinMax:
            return QVariant(QVariant::Invalid);
            break;
        }
        break;
    case Enums::DurationType:
        switch ((Enums::TimeRelationships)(s)) {
        case Enums::Longer:
            return QVariant::fromValue(m_fromTime->duration());
            break;
        case Enums::Shorter:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::TimeBetween:
            return QVariant::fromValue(m_fromTime->duration());
            break;
        case Enums::TimeMinMax:
            return QVariant(QVariant::Invalid);
            break;
        }
    case Enums::EnumType:
        QList<QVariant> list;
        auto items = m_enumList->selectedItems();
        foreach (auto it, items) {
            list << it->data(Qt::UserRole);
        }

        return list;

        break;
    }

}

QVariant FilterWidget::value2()
{
    auto t = Enums::type(m_field);
    auto s = m_stmt->currentData().toInt();
    switch (t) {
    case Enums::NumberType:
        switch ((Enums::NumberRelationships)(s)) {
        case Enums::EQ:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::Greater:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::Less:
            return m_toNumber->value();
            break;
        case Enums::NEQ:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::NumberMinMax:
            return QVariant(QVariant::Invalid);
            break;
        }
        break;
    case Enums::DateType:
        switch ((Enums::DateRelationships)(s)) {
        case Enums::Later:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::Earlier:
            return m_endDate->dateTime();
            break;
        case Enums::Between:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::DateMinMax:
            return QVariant(QVariant::Invalid);
            break;
        }
        break;
    case Enums::DurationType:
        switch ((Enums::TimeRelationships)(s)) {
        case Enums::Longer:
            return QVariant(QVariant::Invalid);
            break;
        case Enums::Shorter:
            return QVariant::fromValue(m_toTime->duration());
            break;
        case Enums::TimeBetween:
            return QVariant::fromValue(m_toTime->duration());
            break;
        case Enums::TimeMinMax:
            return QVariant(QVariant::Invalid);
            break;
        }

    }
}

bool FilterWidget::isMinMax()
{
    auto s = m_stmt->currentData().toInt();
    return (s == Enums::NumberMinMax ||
            s == Enums::TimeMinMax ||
            s == Enums::DateMinMax);

}


}
