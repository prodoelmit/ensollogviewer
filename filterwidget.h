#ifndef FILTERWIDGET_H
#define FILTERWIDGET_H
#include "duration.h"
#include <QComboBox>
#include <QWidget>
#include <QLabel>
#include "enums.h"
#include <QDateTimeEdit>
#include <QSpinBox>
#include <QSet>
#include <QListWidget>
#include <QHBoxLayout>
#include <QPushButton>


namespace logviewer {
class FilterWidget: public QWidget
{
    Q_OBJECT
public:
    FilterWidget(Enums::CurDatumField f);
    Enums::CurDatumField field();
    int statementInt();
    QVariant value1();
    QVariant value2();
    bool isMinMax();
public slots:
    void updateFilterInputs();
private:

    void initLabel();
    void initStatements();
    void initStatements(QString s);
    void initEdits();
    void showEdit(QWidget* w);
    void showEdits(QSet<QWidget*> ws);

private:
    QPushButton* m_destroyButton;
    QLabel* m_label;
    QComboBox* m_stmt;
    QDateTimeEdit* m_startDate;
    QDateTimeEdit* m_endDate;
    DurationEdit* m_fromTime;
    DurationEdit* m_toTime;
    QLineEdit* m_lineEdit;
    QSpinBox* m_fromNumber;
    QSpinBox* m_toNumber;
    QSpinBox* m_eqNumber;
    QListWidget* m_enumList;
    Enums::CurDatumField m_field;
    QSet<QWidget*> m_edits;
    QHBoxLayout* m_layout;

};

}

#endif // FILTERWIDGET_H
