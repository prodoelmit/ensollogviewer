#include "lifedatum.h"
#include "cstdio"
#include "qdebug.h"
#include <QDataStream>
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;
namespace logviewer{

LifeDatum::LifeDatum(QDataStream &in)
{
    in >> m_numrecord;
    in >> m_batteryId;
    in >> m_month;
    in >> m_year;
    in >> m_nomCap;
    in >> m_monthCap;
    in >> m_cyclesInCurMonth
            >> m_cyclesTotal
            >> m_energyMonth
            >> m_energyTotal
            >> m_depthMonth
            >> m_depthTotal
            >> m_speed1000
            >> m_speedMonth
            >> m_currMonth
            >> m_tempMin;
	for (int i = 0; i < 16; i++) {
		in >> m_cellResistance[i];
	}

    qDebug() << m_currMonth << m_tempMin;

}

bsoncxx::builder::stream::document LifeDatum::toBsonDoc()
{
	bsoncxx::builder::stream::document doc;
	doc << "batteryId" << m_batteryId;

	doc << "numRecord" << m_numrecord;

	doc << "monthsSinceEpoch" << monthsSinceEpoch();
	doc << "capacity" << open_document
		<< "nom" << m_nomCap
		<< "month" << m_monthCap
		<< close_document;

	doc << "cycles" << open_document
		<< "month" << (qint32)m_cyclesInCurMonth
		<< "total" << (qint32)m_cyclesTotal
		<< close_document;

	doc << "energy" << open_document
		<< "month" << (qint32)m_energyMonth
		<< "total" << (qint32)m_energyTotal
		<< close_document;

	doc << "cycleDepth" << open_document
		<< "month" << m_depthMonth
		<< "total" << m_depthTotal
		<< close_document;

	doc << "speed" << open_document
		<< "per1000" << (qint32)m_speed1000
		<< "month" << (qint32)m_speedMonth
		<< close_document;

	doc << "currMonth" << m_currMonth;

	doc << "tempMin" << m_tempMin;

	auto doc1 = doc << "cellResistance" << open_array;

	for (int i = 0; i < 16; i++) {
		doc1 << m_cellResistance[i];
	}

	doc1 << close_array;


	return doc;



}

QDate LifeDatum::date() const
{
	return QDate(m_year + 2000, m_month, 1);

}

QDateTime LifeDatum::datetime() const
{
	return QDateTime(date(), QTime(0,0,0));

}

int LifeDatum::monthsSinceEpoch() const
{
	return (m_year + 2000 - 1970) * 12 + (m_month - 1);

}

quint32 LifeDatum::cyclesInCurMonth() const
{
	return m_cyclesInCurMonth;
}

quint16 LifeDatum::batteryId() const
{
	return m_batteryId;
}

quint16 LifeDatum::numrecord() const
{
	return m_numrecord;
}

quint32 LifeDatum::cyclesTotal() const
{
	return m_cyclesTotal;
}

quint32 LifeDatum::energyMonth() const
{
	return m_energyMonth;
}

quint32 LifeDatum::energyTotal() const
{
	return m_energyTotal;
}

quint8 LifeDatum::depthMonth() const
{
	return m_depthMonth;
}

quint8 LifeDatum::depthTotal() const
{
	return m_depthTotal;
}

quint32 LifeDatum::speed1000() const
{
	return m_speed1000;
}

quint32 LifeDatum::speedMonth() const
{
	return m_speedMonth;
}

quint16 LifeDatum::currMonth() const
{
	return m_currMonth;
}

quint8 LifeDatum::tempMin() const
{
	return m_tempMin;
}

}
