#ifndef LIFEDATUM_H
#define LIFEDATUM_H
#include <Qt>
#include <QDateTime>
#include <bsoncxx/builder/stream/document.hpp>

namespace logviewer{
class LifeDatum
{
public:
    LifeDatum(QDataStream& in);

	bsoncxx::builder::stream::document toBsonDoc();

	QDate date() const;
	QDateTime datetime() const;
	int monthsSinceEpoch() const;
	quint16 nomCap() const;
	quint16 monthCap() const;

	quint32 cyclesInCurMonth() const;

	quint16 batteryId() const;

	quint16 numrecord() const;

	quint32 cyclesTotal() const;

	quint32 energyMonth() const;

	quint32 energyTotal() const;

	quint8 depthMonth() const;

	quint8 depthTotal() const;

	quint32 speed1000() const;

	quint32 speedMonth() const;

	quint16 currMonth() const;

	quint8 tempMin() const;



private:
	quint16 m_numrecord;
	quint16 m_batteryId;
    quint8 m_month;
    quint8 m_year;
    quint16 m_nomCap;
    quint16 m_monthCap;
    quint32 m_cyclesInCurMonth;
    quint32 m_cyclesTotal;
    quint32 m_energyMonth;
    quint32 m_energyTotal;
    quint8 m_depthMonth;
    quint8 m_depthTotal;
    quint32 m_speed1000;
    quint32 m_speedMonth;
    quint16 m_currMonth;
    quint8 m_tempMin;
	quint8 m_cellResistance[16]; //Ohm * 10^-5

};



}

#endif // LIFEDATUM_H
