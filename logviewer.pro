#-------------------------------------------------
#
# Project created by QtCreator 2016-09-11T19:07:15
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = logviewer
TEMPLATE = app


include(3rdparty/qtxlsx/src/xlsx/qtxlsx.pri)
SOURCES += main.cpp\
        mw.cpp \
    database.cpp \
    lvapp.cpp \
    lifedatum.cpp \
    curdatum.cpp \
    tablepane.cpp \
    curdatatable.cpp \
    enums.cpp \
    filterwidget.cpp \
    columnselectorwidget.cpp \
    batteriestablewindow.cpp \
    battery.cpp \
    duration.cpp \
    chartwindow.cpp \
    tablepanewindow.cpp \
    dischargestatstable.cpp \
    dischargestatswindow.cpp

HEADERS  += mw.h \
    database.h \
    lvapp.h \
    lifedatum.h \
    curdatum.h \
    tablepane.h \
    curdatatable.h \
    enums.h \
    filterwidget.h \
    columnselectorwidget.h \
    batteriestablewindow.h \
    battery.h \
    duration.h \
    chartwindow.h \
    tablepanewindow.h \
    dischargestatstable.h \
    dischargestatswindow.h

unix: {
LIBS += -L/usr/local/lib -lmongocxx -lbsoncxx
INCLUDEPATH += /usr/local/include/mongocxx/v_noabi \
        /usr/local/include/bsoncxx/v_noabi
}
win32: {
message(asdlkjqwe)
#LIBS += -LC:/local/boost_1_61_0/lib64_msvc-14.0 -lboost_system-vc140-mt-1_61
INCLUDEPATH += C:/local/boost_1_61_0
LIBS += -LC:/usr/lib -lmongocxx -lbsoncxx
INCLUDEPATH += C:/usr/include/mongocxx/v_noabi \
                    C:/usr/include/bsoncxx/v_noabi
Debug::reqs.path = $$OUT_PWD/debug
Release: reqs.path = $$OUT_PWD/release
reqs.files = C:\Qt\5.7\msvc2015_64\plugins\platforms \
        C:\Qt\5.7\msvc2015_64\bin\Qt5Core.dll \
        C:\Qt\5.7\msvc2015_64\bin\Qt5Cored.dll \
        C:\Qt\5.7\msvc2015_64\bin\Qt5Chartsd.dll \
        C:\Qt\5.7\msvc2015_64\bin\Qt5Widgetsd.dll \
        C:\Qt\5.7\msvc2015_64\bin\Qt5Guid.dll \
        ucrtbased.dll \
        ucrtbase.dll \
        C:\usr\bin\bsoncxx.dll \
        C:\usr\bin\mongocxx.dll \
        C:\usr\bin\libmongoc-1.0.dll \
        C:\usr\bin\libbson-1.0.dll \

INSTALLS += reqs



}


