#include "lvapp.h"
#include "dischargestatswindow.h"
#include "tablepanewindow.h"
#include "chartwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include "batteriestablewindow.h"
#include "duration.h"

namespace logviewer {

LVApp::LVApp(int& argc, char** argv)
    : QApplication(argc, argv)
{
    setStyleSheet("QMainWindow, QSplitter { background-color: #d0e5ed }");
    qRegisterMetaType<logviewer::Duration>("Duration");

}

Database *LVApp::database() const
{
    return m_db;

}

void LVApp::importCurData()
{

    QString currentFilename = QFileDialog::getOpenFileName(0, "Выберите файл CURRENT.LOG", "", "*.LOG");
	if (!(currentFilename.length())) {
		auto mbox = new QMessageBox();
        mbox->setText("Файл не выбран");
		mbox->exec();
		return;
	}

	m_db->importCurData(currentFilename);
	m_db->exportCurDataToMongo();

}

void LVApp::showCurData(int battery_id)
{

	auto p = new TablePane(battery_id);
    auto pw = new TablePaneWindow(p);
    pw->showMaximized();

}

void LVApp::showChart(int battery_id)
{
    auto w = new ChartWindow(battery_id);
    w->showMaximized();

}

void LVApp::showDischargeStats(int battery_id)
{

    DischargeStatsWindow* w = new DischargeStatsWindow(battery_id);
    w->show();
//    w->prepareData();
//    w->fillTable();

}

void LVApp::importLifeData()
{
    QString lifeFilename = QFileDialog::getOpenFileName(0, "Выберите файл life.LOG", "", "*.LOG");
	if (!(lifeFilename.length())) {
		auto mbox = new QMessageBox();
        mbox->setText("Выбранный файл не может быть прочитан");
		mbox->exec();
		return;
	}

	m_db->importLifeData(lifeFilename);
	m_db->exportLifeDataToMongo();

}

void LVApp::run()
{

    m_db = new Database();


    auto bw = new BatteriesTableWindow();
	bw->showMaximized();

//	importLifeData();
//	m_db->exportLifeDataToMongo();





}

}
