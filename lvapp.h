#ifndef LVAPP_H
#define LVAPP_H
#include <QApplication>
#include "tablepane.h"

#include "database.h"


namespace logviewer{
class Database;
class LVApp: public QApplication
{
    Q_OBJECT
public:
    LVApp(int &argc, char **argv);
    Database* database() const;
	void importCurData();
	void showCurData(int battery_id);
    void showChart(int battery_id);
    void showDischargeStats(int battery_id);
    void importLifeData();
public slots:
    void run();


private:
    Database* m_db;
    TablePane* m_tablePane;
};
}

#define app dynamic_cast<LVApp*>(qApp->instance())
#define db app->database()
#define mdb db->mongoDB()
#endif // LVAPP_H
