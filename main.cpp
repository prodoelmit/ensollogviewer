#include "mw.h"
#include <QApplication>
#include "lvapp.h"
#include <QTimer>
using namespace logviewer;

int main(int argc, char *argv[])
{
    LVApp* a = new LVApp(argc, argv);

    QTimer::singleShot(0, a, SLOT(run()));
    return a->exec();
}
