#include "tablepane.h"
#include <QGridLayout>
#include "lvapp.h"
#include "qdebug.h"

namespace logviewer {
TablePane::TablePane(int batteryId)
    : QSplitter()
{
//	setStyleSheet("background-color: #d0e5ed");
    setWindowTitle(QString("Сводная таблица событий батареи %1").arg(batteryId));

    setAutoFillBackground(true);
    setOrientation(Qt::Vertical);
    auto cLayout = new QGridLayout();
    setLayout(cLayout);

    m_tableWidget = new CurDataTable;
//	m_tableWidget->fillWithData(app->database());
    auto v = db->queryCurData(batteryId);
    qDebug() << v;
    m_tableWidget->fillWithData(v);
//    m_tableWidget->setVisibleColumns(QSet<int>() << 0 << 3 << 5);

    cLayout->addWidget(m_tableWidget, 0, 0);

    initFiltersWrapper();

    cLayout->addWidget(m_filtersWrapperWidget, 1,0);

    initColumnSelectionWidget();

    cLayout->addWidget(m_columnSelectionWrapperWidget, 2, 0);


}

QSize TablePane::sizeHint() const
{
    return QSize(1000, 500);

}

void TablePane::initFiltersWrapper()
{
    m_filtersWrapperLayout = new QGridLayout();
    m_filtersWrapperWidget = new QGroupBox();
    m_filtersWrapperWidget->setTitle(tr("Фильтры"));
    m_filtersWrapperWidget->setLayout(m_filtersWrapperLayout);

    m_addFilterLabel = new QLabel(tr("Добавить фильтр"));
    m_addFilterCombobox = new QComboBox();

    //add items to combobox
    {
        m_addFilterCombobox->addItem("",0);
        m_addFilterCombobox->addItem(tr( "Состояние"),Enums::StateCode);
        m_addFilterCombobox->addItem(tr( "Начало"),Enums::StartDatetime);
        m_addFilterCombobox->addItem(tr( "Конец"),Enums::StopDatetime);
        m_addFilterCombobox->addItem(tr( "Длительность"),Enums::Duration);
        m_addFilterCombobox->addItem(tr( "Начальный заряд"),Enums::StartCharge);
        m_addFilterCombobox->addItem(tr( "Конечный заряд"),Enums::EndCharge);
        m_addFilterCombobox->addItem(tr( "Разность зарядов"),Enums::DiffCharge);
        m_addFilterCombobox->addItem(tr( "Начальное напряжение"),Enums::StartVoltage);
        m_addFilterCombobox->addItem(tr( "Конечное напряжение"),Enums::EndVoltage);
        m_addFilterCombobox->addItem(tr( "Разность напряжений"),Enums::DiffVoltage);
        m_addFilterCombobox->addItem(tr( "Начальная степень разряда"),Enums::StartDischargeDegree);
        m_addFilterCombobox->addItem(tr( "Конечная степень разряда"),Enums::EndDischargeDegree);
        m_addFilterCombobox->addItem(tr( "Разность степени разряда"),Enums::DiffDischargeDegree);
        m_addFilterCombobox->addItem(tr( "Емкость"),Enums::MinCapacity);
        m_addFilterCombobox->addItem(tr( "Эффективная емкость"),Enums::MinEffCapacity);
        m_addFilterCombobox->addItem(tr( "Ячейка с макс.температурой"),Enums::MaxTempCellId);
        m_addFilterCombobox->addItem(tr( "Макс. температура"),Enums::MaxTemp);
        m_addFilterCombobox->addItem(tr( "Ток"),Enums::MaxCurrent);
        m_addFilterCombobox->addItem(tr( "Ячейка с минимальным напряжением"),Enums::MinVoltageCellId);
        m_addFilterCombobox->addItem(tr( "Минимальное напряжение"),Enums::MinVoltage);
        m_addFilterCombobox->addItem(tr( "Ячейка с максимальным напряжением"),Enums::MaxVoltageCellId);
        m_addFilterCombobox->addItem(tr( "Максимальное напряжение"),Enums::MaxVoltage);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 1"),Enums::CellVoltage1);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 2"),Enums::CellVoltage2);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 3"),Enums::CellVoltage3);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 4"),Enums::CellVoltage3);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 5"),Enums::CellVoltage5);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 6"),Enums::CellVoltage6);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 7"),Enums::CellVoltage7);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 8"),Enums::CellVoltage8);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 9"),Enums::CellVoltage9);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 10"),Enums::CellVoltage10);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 11"),Enums::CellVoltage11);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 12"),Enums::CellVoltage12);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 13"),Enums::CellVoltage13);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 14"),Enums::CellVoltage14);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 15"),Enums::CellVoltage15);
        m_addFilterCombobox->addItem(tr( "Напряжение в ячейке 16"),Enums::CellVoltage16);
        m_addFilterCombobox->addItem(tr( "Мин.температура логгера"),Enums::MinLoggerTemp);
        m_addFilterCombobox->addItem(tr( "Макс.температура логгера"),Enums::MinLoggerTemp);
        m_addFilterCombobox->addItem(tr( "Ошибки"),Enums::ErrorCode);


}


    m_filtersWrapperLayout->addWidget(m_addFilterLabel, 0, 0);
    m_filtersWrapperLayout->addWidget(m_addFilterCombobox, 0, 1);

    connect( m_addFilterCombobox,
             static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
             [this]() {
        auto v = m_addFilterCombobox->currentData();
        if (v.toInt()) {
            addFilterView((Enums::CurDatumField)(v.toInt()));
            m_addFilterCombobox->setCurrentIndex(0);
        }
    });


    m_applyFilterButton = new QPushButton(tr("Применить фильтры"));
//    m_applyFilterButton->setStyleSheet("background-color: green;");
    m_filtersWrapperLayout->addWidget(m_applyFilterButton, 0, 3);

    connect(m_applyFilterButton, &QPushButton::clicked,
            this, &TablePane::applyFilters);




}

void TablePane::initColumnSelectionWidget()
{
    m_columnSelectionWrapperWidget = new QGroupBox();
    m_columnSelectionWrapperLayout = new QVBoxLayout();
    m_columnSelectionWrapperWidget->setLayout(m_columnSelectionWrapperLayout);
//    m_columnSelectionWrapperWidget->setTitle(tr("Выберите столбцы для отображения"));


    auto fields = Enums::allFieldsList();
    fields.removeOne(Enums::BatteryId);
    m_columnSelectionWidget = new ColumnSelectorWidget(fields);
//    connect(m_columnSelectionWidget, &ColumnSelectorWidget::columnsChanged,
//            m_tableWidget, &CurDataTable::setVisibleColumns);
    connect(m_columnSelectionWidget, SIGNAL(columnsChanged(QSet<Enums::CurDatumField>)),
            m_tableWidget, SLOT(setVisibleColumns(QSet<Enums::CurDatumField>)));

    m_columnSelectionWrapperLayout->addWidget(m_columnSelectionWidget, 0, 0);




}

void TablePane::addFilterView(Enums::CurDatumField f)
{

    auto w = new FilterWidget(f);
    m_filterWidgets << w;
    m_filtersWrapperLayout->addWidget(w, m_filtersWrapperLayout->rowCount(), 0, 1, 2);

    connect(w, &FilterWidget::destroyed,
            [this, w](){
        m_filterWidgets.removeAll(w);
    });



}

void TablePane::applyFilters()
{
    m_tableWidget->clearFilters();

    // sorting for minmax filters to be applied last
    std::stable_sort(m_filterWidgets.begin(), m_filterWidgets.end(), [](FilterWidget* f1, FilterWidget* f2) {
       return f1->isMinMax() < f2->isMinMax();
    } );

    foreach (auto fw, m_filterWidgets) {
        auto f = fw->field();
        auto s = fw->statementInt();
        auto v1 = fw->value1();
        auto v2 = fw->value2();
        m_tableWidget->applyFilter(f, s, v1, v2);
    }
    m_tableWidget->showMinMaxRows();

}

void TablePane::exportToXlsx()
{
    m_tableWidget->exportToXlsx();
}

}
