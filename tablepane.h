#ifndef TABLEPANE_H
#define TABLEPANE_H
#include <QWidget>
#include <QGridLayout>
#include "curdatatable.h"
#include <QGroupBox>
#include <QLabel>
#include <QComboBox>
#include "filterwidget.h"
#include <QPushButton>
#include "columnselectorwidget.h"
#include <QSplitter>

namespace logviewer {
class TablePane: public QSplitter
{
    Q_OBJECT
public:
	TablePane(int batteryId);
    QSize sizeHint() const;

    void initFiltersWrapper();
    void initColumnSelectionWidget();
public slots:
    void addFilterView(Enums::CurDatumField f);
    void applyFilters();
    void exportToXlsx();

private:
    CurDataTable* m_tableWidget;
    QGroupBox* m_filtersWrapperWidget;
    QGridLayout* m_filtersWrapperLayout;

    QLabel* m_addFilterLabel;
    QComboBox* m_addFilterCombobox;

    QPushButton* m_applyFilterButton;

    QList<FilterWidget*> m_filterWidgets;

    QGroupBox* m_columnSelectionWrapperWidget;
    QVBoxLayout* m_columnSelectionWrapperLayout;

    ColumnSelectorWidget* m_columnSelectionWidget;




};

}
#endif // TABLEPANE_H
