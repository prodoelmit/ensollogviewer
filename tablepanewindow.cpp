#include "tablepanewindow.h"
#include <QMenuBar>
#include <QAction>

TablePaneWindow::TablePaneWindow(TablePane *pane):
    QMainWindow()
{
    setWindowTitle(pane->windowTitle());
    m_pane = pane;
    setCentralWidget(pane);

    m_exportToXlsxAction = new QAction("Экспортировать в Excel");
    connect(m_exportToXlsxAction, &QAction::triggered,
            m_pane, &TablePane::exportToXlsx );

    menuBar()->addAction(m_exportToXlsxAction);


}
