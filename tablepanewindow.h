#ifndef TABLEPANEWINDOW_H
#define TABLEPANEWINDOW_H
#include "tablepane.h"

#include <QMainWindow>

class TablePaneWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit TablePaneWindow(TablePane* pane);
private:
    TablePane* m_pane;
    QAction *m_exportToXlsxAction;

signals:

public slots:
};

#endif // TABLEPANEWINDOW_H
